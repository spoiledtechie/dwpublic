﻿using System.IO;
using System.Linq;
using DW.Library.Classes;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;

namespace DW.DocumentHelper
{
    public class PDF
    {
        private static readonly BaseColor lightestGray = new BaseColor(240, 240, 240);
        private static readonly BaseColor lightGray = new BaseColor(225, 225, 225);
        private static readonly BaseColor gray = BaseColor.GRAY;
        private static readonly BaseColor darkGray = new BaseColor(142, 142, 142);

        private static readonly int paddingBottomLastTableCell = 5;
        private static readonly int paddingBottomFirstTableCell = 5;
        private static readonly int groupBoxTitleSize = 12;

        public static bool GeneratePDF(MemoryStream stream, DealerVM currentDealer, DealerVM initialDealer, QuarterlyTiersVM tiers)
        {
            ResultVM currentResult = currentDealer.Result;
            ResultVM initialResult = initialDealer.InitialResult;
            string title = currentDealer.Name;
            decimal cusPayRo = currentResult.CustPayRoVsPriorYr;
            decimal cusPay = currentResult.CustPayMoneyVsPriortYr;

            Document document = new Document(PageSize.LETTER, 50, 50, 30, 20);
            try
            {
                PdfWriter.GetInstance(document, stream).CloseStream = false;
                document.Open();

                //metadata
                document.AddTitle("Client report");
                document.AddSubject("Report");
                //document.AddKeywords("report, client");
                document.AddCreator("DealerWing");
                document.AddAuthor("");
                document.AddHeader("Expires", "0");

                //title
                Paragraph titleParagraph = new Paragraph(title);
                titleParagraph.Alignment = Element.ALIGN_CENTER;
                titleParagraph.Font.Size = 18;
                document.Add(titleParagraph);

                Paragraph subtitleParagraph = new Paragraph("DEALERWING results for " + currentResult.MonthDateTime.ToString("MMMM") + " " + currentResult.MonthDateTime.Year);
                subtitleParagraph.Alignment = Element.ALIGN_CENTER;
                subtitleParagraph.Font.Size = 10;
                document.Add(subtitleParagraph);

                //table for groupboxes
                PdfPTable documentTable = new PdfPTable(1);
                documentTable.WidthPercentage = 100;
                documentTable.HorizontalAlignment = Element.ALIGN_CENTER;

                #region Dealer Reentetion Status        2 columns 
                PdfPTable table = new PdfPTable(2);
                PdfPCell cell;
                table.WidthPercentage = 100;
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                Paragraph paragraph = new Paragraph();
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Add(new Chunk("INITIAL NUMBERS", FontFactory.GetFont(FontFactory.HELVETICA, 15, Font.BOLD)));
                paragraph.Add("\n\n");
                paragraph.Add(new Chunk("START DATE " + initialResult.MonthDateTime.Month + "/" + initialResult.MonthDateTime.Year, FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL)));

                cell = new PdfPCell(paragraph);
                cell.BorderWidth = 0;
                cell.Padding = 0;
                //cell.PaddingTop = 12;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);


                paragraph = new Paragraph();
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Add(new Chunk("CURRENT NUMBERS", FontFactory.GetFont(FontFactory.HELVETICA, 15, Font.BOLD)));
                paragraph.Add("\n\n");
                paragraph.Add(new Chunk("REPORT DATE " + currentResult.MonthDateTime.Month + "/" + currentResult.MonthDateTime.Year, FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL)));
                cell = new PdfPCell(paragraph);
                cell.BorderWidth = 0;
                cell.Padding = 0;
                //cell.PaddingTop = 12;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                table.AddCell(cell);

                #region small tables ( INITIAL and CURRENT numbers)

                var innerTable = GetTableCellStyle1(initialResult);
                //add this table to main outerTable as a cell
                PdfPCell tableCell = new PdfPCell();
                tableCell.BorderWidth = 0;
                tableCell.Padding = 0;
                tableCell.PaddingTop = 10;
                tableCell.PaddingRight = 10;
                tableCell.AddElement(innerTable);
                table.AddCell(tableCell);

                // second (right) cell/ innerTable
                innerTable = GetTableCellStyle1(currentResult);

                tableCell = new PdfPCell();
                tableCell.BorderWidth = 0;
                tableCell.Padding = 0;
                tableCell.PaddingTop = 10;
                tableCell.PaddingLeft = 10;
                tableCell.AddElement(innerTable);
                table.AddCell(tableCell);
                #endregion

                #region CHANGE SINCE INCEPTION
                paragraph = new Paragraph();
                paragraph.Alignment = Element.ALIGN_RIGHT;
                paragraph.Add(new Chunk("CHANGE SINCE INCEPTION", FontFactory.GetFont(FontFactory.HELVETICA, 15, Font.BOLD, gray)));
                cell = new PdfPCell(paragraph);
                cell.BorderWidth = 0;
                cell.Padding = 0;
                cell.PaddingTop = 12;
                cell.PaddingRight = 10;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                table.AddCell(cell);

                //gray table 4 columns
                innerTable = new PdfPTable(4);
                innerTable.WidthPercentage = 100;
                innerTable.HorizontalAlignment = Element.ALIGN_CENTER;

                innerTable.AddCell(GetCell((currentResult.Overall - initialResult.Overall).ToString("N2"), lightGray, darkGray, 10, gray));
                innerTable.AddCell(GetCell((currentResult.FirstYear - initialResult.FirstYear).ToString("N2"), lightGray, darkGray, 10, gray));
                innerTable.AddCell(GetCell((currentResult.InWar - initialResult.InWar).ToString("N2"), lightGray, darkGray, 10, gray));
                innerTable.AddCell(GetCell((currentResult.OutWar - initialResult.OutWar).ToString("N2"), lightGray, darkGray, 10, gray));

                tableCell = new PdfPCell(innerTable) { BorderWidth = 0 };
                tableCell.Padding = 0;
                tableCell.PaddingTop = 12;
                tableCell.PaddingLeft = 10;
                table.AddCell(tableCell);
                #endregion

                //add this groupbox
                AddGroupBoxCell(documentTable, table, "Dealer Retention Status");

                //document.Add(table);
                #endregion

                #region Dealer RO Status LIGHT_GRAY table -> CUST PAY RO'S...

                table = new PdfPTable(2);
                //table.SpacingBefore = 20;
                //table.SpacingAfter = 20;
                table.WidthPercentage = 100;
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                //CUST PAY RO'S
                paragraph = new Paragraph();
                paragraph.Alignment = Element.ALIGN_RIGHT;
                paragraph.Add(new Chunk("CUST PAY RO'S VS PRIOR YR=  ", FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD)));
                paragraph.Add(new Chunk(string.Format("{0:0.0#}", cusPayRo) + "%", FontFactory.GetFont(FontFactory.HELVETICA, 11, Font.BOLD, gray)));
                cell = new PdfPCell(paragraph);
                cell.BorderWidth = 0;
                cell.Padding = 0;
                cell.PaddingTop = 12;
                cell.PaddingBottom = 12;
                cell.PaddingRight = 10;
                cell.BackgroundColor = lightestGray;
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;

                table.AddCell(cell);

                //CUST PAY $$
                paragraph = new Paragraph();
                paragraph.Alignment = Element.ALIGN_RIGHT;
                paragraph.Add(new Chunk($"CUST PAY $$ VS PRIOR YR=  ", FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD)));
                paragraph.Add(new Chunk(string.Format("{0:0.0#}", cusPay) + "%", FontFactory.GetFont(FontFactory.HELVETICA, 11, Font.BOLD, gray)));
                cell = new PdfPCell(paragraph);
                cell.BorderWidth = 0;
                cell.Padding = 0;
                cell.PaddingTop = 12;
                cell.PaddingBottom = 12;
                cell.PaddingLeft = 10;
                cell.BackgroundColor = lightestGray;
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                table.AddCell(cell);

                AddGroupBoxCell(documentTable, table, "Dealer RO Status");
                //document.Add(table);

                #endregion

                #region Dealer Data Status

                //Header
                //paragraph = new Paragraph();
                //paragraph.Alignment = Element.ALIGN_CENTER;
                //paragraph.Add(new Chunk("DATA STATUS", FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));
                //paragraph.SpacingAfter = 5;
                //cell = new PdfPCell();
                //cell.Colspan = 3;
                //cell.Padding = 0;
                //cell.BorderWidth = 0;
                //cell.HorizontalAlignment = Element.ALIGN_CENTER;
                //cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                //cell.AddElement(paragraph);
                //table.AddCell(cell);

                #region table
                table = new PdfPTable(5);
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                table.AddCell(GetCell("", fontSize: 10));
                table.AddCell(GetCell("ORIGINAL", fontSize: 10));
                table.AddCell(GetCell("CURRENT", fontSize: 10));
                table.AddCell(GetCell("DIFFERENCE", fontSize: 10));
                table.AddCell(GetCell("% CHANGE", fontSize: 10));

                //data rows
                table.AddCell(GetCell("ACB", borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell(initialResult.AbcOrig.ToString(), borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell(currentResult.AbcNow.ToString(), borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell((currentResult.AbcNow - initialResult.AbcOrig).ToString(), borderColor: lightGray, fontSize: 10));
                if (initialResult.AbcOrig > 0)
                    table.AddCell(GetCell((((decimal)(currentResult.AbcNow - initialResult.AbcOrig) / initialResult.AbcOrig) * 100).ToString("N2") + "%", borderColor: lightGray, fontSize: 10));
                else
                    table.AddCell(GetCell("", borderColor: lightGray, fontSize: 10));

                table.AddCell(GetCell("0-VISIT", borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell(initialResult.ZeroVisitOrig.ToString(), borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell(currentResult.ZeroVisitNow.ToString(), borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell((currentResult.ZeroVisitNow - initialResult.ZeroVisitOrig).ToString(), borderColor: lightGray, fontSize: 10));

                if (initialResult.ZeroVisitOrig > 0)
                    table.AddCell(GetCell((((decimal)(currentResult.ZeroVisitNow - initialResult.ZeroVisitOrig) / initialResult.ZeroVisitOrig) * 100).ToString("N2") + "%", borderColor: lightGray, fontSize: 10));
                else
                    table.AddCell(GetCell("", borderColor: lightGray, fontSize: 10));

                table.AddCell(GetCell("2+VISIT", borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell(initialResult.TwoVisitOrig.ToString(), borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell(currentResult.TwoVisitNow.ToString(), borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell((currentResult.TwoVisitNow - initialResult.TwoVisitOrig).ToString(), borderColor: lightGray, fontSize: 10));
                if (initialResult.TwoVisitOrig > 0)
                    table.AddCell(GetCell((((decimal)(currentResult.TwoVisitNow - initialResult.TwoVisitOrig) / initialResult.TwoVisitOrig) * 100).ToString("N2") + "%", borderColor: lightGray, fontSize: 10));
                else
                    table.AddCell(GetCell("", borderColor: lightGray, fontSize: 10));

                table.AddCell(GetCell("Cross Svc", borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell(initialResult.CrossOrig.ToString(), borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell(currentResult.CrossNow.ToString(), borderColor: lightGray, fontSize: 10));
                table.AddCell(GetCell((currentResult.CrossNow - initialResult.CrossOrig).ToString(), borderColor: lightGray, fontSize: 10));
                if (initialResult.CrossOrig > 0)
                    table.AddCell(GetCell((((decimal)(currentResult.CrossNow - initialResult.CrossOrig) / initialResult.CrossOrig) * 100).ToString("N2") + "%", borderColor: lightGray, fontSize: 10));
                else
                    table.AddCell(GetCell("", borderColor: lightGray, fontSize: 10));

                //document.Add(table);
                AddGroupBoxCell(documentTable, table, "Dealer Data Status");
                #endregion

                #endregion

                #region Dealer Retention tier Status

                PdfPTable tierTable = new PdfPTable(3);
                tierTable.SetWidths(new float[] { 200F, 75F, 200F });

                tierTable.WidthPercentage = 100;
                tierTable.HorizontalAlignment = Element.ALIGN_CENTER;

                //tier levels


                paragraph = new Paragraph();
                paragraph.PaddingTop = 20;
                paragraph.SpacingBefore = 20;
                paragraph.Alignment = Element.ALIGN_RIGHT;
                paragraph.Add(new Chunk($"TIER LEVELS - IN WARRANTY:", FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));

                PdfPCell tierCell = new PdfPCell(paragraph);
                tierCell.BorderWidth = 0;
                tierCell.Padding = 0;
                tierCell.NoWrap = true;
                tierCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                tierTable.AddCell(tierCell);

                paragraph = new Paragraph();
                paragraph.PaddingTop = 20;
                paragraph.SpacingBefore = 20;
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Add(new Chunk($"Tier - 1 - {tiers.INWT1}", FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));

                tierCell = new PdfPCell(paragraph);
                tierCell.BorderWidth = 0;
                tierCell.Padding = 0;
                tierCell.HorizontalAlignment = Element.ALIGN_CENTER;
                tierTable.AddCell(tierCell);

                paragraph = new Paragraph();
                paragraph.PaddingTop = 20;
                paragraph.SpacingBefore = 20;
                paragraph.Alignment = Element.ALIGN_LEFT;
                paragraph.Add(new Chunk($"/ Tier - 2 - {tiers.INWT2}", FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));

                tierCell = new PdfPCell(paragraph);
                tierCell.BorderWidth = 0;
                tierCell.Padding = 0;
                //cell.PaddingTop = 12;
                tierCell.HorizontalAlignment = Element.ALIGN_LEFT;
                tierTable.AddCell(tierCell);

                paragraph = new Paragraph();
                paragraph.PaddingTop = 20;
                paragraph.SpacingBefore = 20;
                paragraph.Alignment = Element.ALIGN_RIGHT;
                paragraph.Add(new Chunk($"TIER LEVELS - OUT OF WARRANTY:", FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));

                tierCell = new PdfPCell(paragraph);
                tierCell.BorderWidth = 0;
                tierCell.Padding = 0;
                tierCell.PaddingTop = 5;
                tierCell.NoWrap = true;
                tierCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                tierTable.AddCell(tierCell);

                paragraph = new Paragraph();
                paragraph.PaddingTop = 20;
                paragraph.SpacingBefore = 20;
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Add(new Chunk($"Tier - 1 - {tiers.OOWT1}", FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));

                tierCell = new PdfPCell(paragraph);
                tierCell.BorderWidth = 0;
                tierCell.Padding = 0;
                tierCell.PaddingTop = 5;
                //cell.PaddingTop = 12;
                tierCell.HorizontalAlignment = Element.ALIGN_CENTER;
                tierTable.AddCell(tierCell);

                paragraph = new Paragraph();
                paragraph.PaddingTop = 20;
                paragraph.SpacingBefore = 20;
                paragraph.Alignment = Element.ALIGN_LEFT;
                paragraph.Add(new Chunk($"/ Tier - 2 - {tiers.OOWT2}", FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));

                tierCell = new PdfPCell(paragraph);
                tierCell.BorderWidth = 0;
                tierCell.Padding = 0;
                tierCell.PaddingTop = 5;
                //cell.PaddingTop = 12;
                tierCell.HorizontalAlignment = Element.ALIGN_LEFT;
                tierTable.AddCell(tierCell);

                //document.Add(paragraph);
                AddGroupBoxCell(documentTable, tierTable, "Dealer Retention Tier Status");
                #endregion

                //add all groupboxes
                document.Add(documentTable);

                #region gray table - 7 table cells
                table = new PdfPTable(4);
                float[] widths = new float[] { 30f, 40f, 25f, 25f };
                table.SetWidths(widths);
                table.WidthPercentage = 100;
                table.SpacingBefore = 15;

                //ORIG-TIER STATUS row 1
                table.AddCell(new PdfPCell(GetTableCellStyle2("ORIG-TIER STATUS", "IN WAR", "OUT WAR", "N/A", "N/A"))
                {
                    BorderWidth = 4,
                    BorderColor = BaseColor.WHITE
                });

                table.AddCell(new PdfPCell(GetTableCellStyle2("CURRENT TIER STATUS", "IN WAR", "OUT WAR", "N/A", "N/A", true))
                {
                    BorderWidth = 4,
                    BorderColor = BaseColor.WHITE
                });
                table.AddCell(new PdfPCell(GetTableCellStyle3("BONUS: PER", "N/A", "CAR/QTR", lastIsGray: true))
                {
                    BorderWidth = 4,
                    BorderColor = BaseColor.WHITE
                });
                table.AddCell(new PdfPCell(GetTableCellStyle3("QTR PAYOUT", "N/A", "(APPROX)", lastIsGray: true))
                {
                    BorderWidth = 4,
                    BorderColor = BaseColor.WHITE
                });

                document.Add(table);
                #endregion

                #region gray table - 7 table cells
                table = new PdfPTable(4);
                table.SetWidths(widths);
                table.WidthPercentage = 100;
                table.SpacingBefore = 15;

                //row 2
                table.AddCell(new PdfPCell(GetTableCellStyle3("NISSAN 10", "N/A", "OOW ELIGIBLE", "(Must be Tier-1 Out-War)"))
                {
                    BorderWidth = 4,
                    BorderColor = BaseColor.WHITE
                });
                table.AddCell(new PdfPCell(GetTableCellStyle3("AGP $$", "N/A", "(APPROX)", spaceAfter: 25))
                {
                    BorderWidth = 4,
                    BorderColor = BaseColor.WHITE
                });

                #region special table for 2 cells
                innerTable = new PdfPTable(1);
                innerTable.WidthPercentage = 100;

                paragraph = new Paragraph();
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Add(new Chunk("TOTAL QTR INCOME FROM\n DEALERWING SERVICES", FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD)));
                paragraph.SpacingAfter = 25;
                cell = new PdfPCell();
                cell.Padding = 0;
                cell.BorderWidth = 0;
                cell.PaddingTop = paddingBottomFirstTableCell;
                cell.BackgroundColor = lightestGray;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.AddElement(paragraph);
                innerTable.AddCell(cell);

                paragraph = new Paragraph();
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Add(new Chunk("N/A", FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD, BaseColor.WHITE)));
                paragraph.SpacingAfter = 5;
                cell = new PdfPCell();
                cell.Padding = 0;
                cell.PaddingBottom = paddingBottomLastTableCell;
                cell.BorderWidth = 0;
                cell.BackgroundColor = darkGray;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.AddElement(paragraph);
                innerTable.AddCell(cell);

                var totalCell = new PdfPCell();
                totalCell.Colspan = 2;
                totalCell.Padding = 0;
                totalCell.BorderWidth = 4;
                totalCell.BorderColor = BaseColor.WHITE;
                totalCell.AddElement(innerTable);

                table.AddCell(totalCell);
                #endregion

                document.Add(table);
                #endregion


            }
            catch (DocumentException e)
            {
                return false;
            }
            catch (IOException e)
            {
                return false;
            }

            document.Close();
            return true;
        }

        /// <summary>
        /// create a cell
        /// </summary>
        private static PdfPCell GetCell(string value, BaseColor bgColor = null, BaseColor borderColor = null, int fontSize = 12, BaseColor fontColor = null)
        {
            var paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            if (fontColor == null)
                fontColor = BaseColor.BLACK;
            paragraph.Add(new Chunk(value, FontFactory.GetFont(FontFactory.HELVETICA, fontSize, Font.BOLD, fontColor)));
            paragraph.SpacingAfter = 5;

            var cell = new PdfPCell();
            cell.Padding = 0;
            cell.BorderWidth = 0;
            if (bgColor != null)
            {
                cell.BackgroundColor = bgColor;
            }
            if (borderColor != null)
            {
                cell.BorderColor = borderColor;
                cell.BorderWidth = 1;
            }
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;

            cell.AddElement(paragraph);
            return cell;
        }

        /// <summary>
        /// build a table with ResultVM content
        /// </summary>
        private static PdfPTable GetTableCellStyle1(ResultVM result)
        {
            PdfPTable table = new PdfPTable(4);
            table.WidthPercentage = 100;
            table.HorizontalAlignment = Element.ALIGN_CENTER;
            //row1
            table.AddCell(GetCell("Overall", borderColor: lightGray, fontSize: 10));
            table.AddCell(GetCell("1st Year", borderColor: lightGray, fontSize: 10));
            table.AddCell(GetCell("In-War", borderColor: lightGray, fontSize: 10));
            table.AddCell(GetCell("Out-War", borderColor: lightGray, fontSize: 10));
            //row2
            table.AddCell(GetCell(result.Overall.ToString("0.00"), borderColor: lightGray, fontSize: 10));
            table.AddCell(GetCell(result.FirstYear.ToString("0.00"), borderColor: lightGray, fontSize: 10));
            table.AddCell(GetCell(result.InWar.ToString("0.00"), borderColor: lightGray, fontSize: 10));
            table.AddCell(GetCell(result.OutWar.ToString("0.00"), borderColor: lightGray, fontSize: 10));

            return table;
        }

        /// <summary>
        /// gray table with 2 columns and 3 rows
        /// </summary>
        private static PdfPTable GetTableCellStyle2(string title, string c11, string c12, string c21, string c22, bool lastIsGray = false)
        {
            var innerTable = new PdfPTable(2);

            var titleCell = GetCell(title, lightestGray);
            titleCell.Colspan = 2;
            titleCell.BorderWidth = 0;
            titleCell.PaddingTop = paddingBottomFirstTableCell;
            innerTable.AddCell(titleCell);

            innerTable.AddCell(GetCell(c11, lightestGray));
            innerTable.AddCell(GetCell(c12, lightestGray));

            var c = GetCell(c21, lightGray);
            c.PaddingBottom = 5;
            innerTable.AddCell(c);
            if (lastIsGray)
            {
                c = GetCell(c22, lightGray, fontColor: gray);
                c.PaddingBottom = paddingBottomLastTableCell;
                innerTable.AddCell(c);
            }
            else
            {
                c = GetCell(c22, lightGray);
                c.PaddingBottom = paddingBottomLastTableCell;
                innerTable.AddCell(c);
            }

            return innerTable;
        }

        /// <summary>
        /// gray table with 1 column and 2 rows
        /// </summary>
        private static PdfPTable GetTableCellStyle3(string title, string c11, string subTitle = "", string subSubtitle = "", int spaceAfter = 10, bool lastIsGray = false)
        {
            var innerTable = new PdfPTable(1);

            if (string.IsNullOrEmpty(subTitle))
            {
                var titleCell = GetCell(title, lightestGray);
                titleCell.Colspan = 2;
                titleCell.PaddingTop = paddingBottomFirstTableCell;
                innerTable.AddCell(titleCell);

                innerTable.AddCell(GetCell(""));
            }
            else //build a special paragraph for cell
            {
                var paragraph = new Paragraph();
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Add(new Chunk(title, FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD)));
                paragraph.Add("\n");
                paragraph.Add(new Chunk(subTitle, FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD)));
                if (!string.IsNullOrEmpty(subSubtitle))
                {
                    paragraph.Add("\n");
                    paragraph.Add(new Chunk(subSubtitle, FontFactory.GetFont(FontFactory.HELVETICA, 7, Font.BOLD)));
                }
                paragraph.SpacingAfter = spaceAfter;

                var cell = new PdfPCell();
                cell.Padding = 0;
                cell.BorderWidth = 0;
                cell.PaddingTop = paddingBottomFirstTableCell;
                cell.BackgroundColor = lightestGray;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                cell.AddElement(paragraph);
                innerTable.AddCell(cell);
            }
            if (lastIsGray)
            {
                var c = GetCell(c11, lightGray, fontColor: gray);
                c.PaddingBottom = paddingBottomLastTableCell;
                innerTable.AddCell(c);
            }
            else
            {
                var c = GetCell(c11, lightGray);
                c.PaddingBottom = paddingBottomLastTableCell;
                innerTable.AddCell(c);
            }

            return innerTable;
        }

        private static void AddGroupBoxCell(PdfPTable documentTable, Paragraph contentParagraph, string title)
        {
            var cell = new PdfPCell(contentParagraph);
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            AddGroupBoxCell(documentTable, cell, title);
        }

        private static void AddGroupBoxCell(PdfPTable documentTable, PdfPTable table, string title)
        {
            var cell = new PdfPCell(table);
            AddGroupBoxCell(documentTable, cell, title);
        }

        private static void AddGroupBoxCell(PdfPTable documentTable, PdfPCell cell, string title)
        {
            var paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            var chunkTitle = new Chunk(title, FontFactory.GetFont(FontFactory.HELVETICA, groupBoxTitleSize, Font.NORMAL));
            chunkTitle.SetBackground(BaseColor.WHITE);
            paragraph.Add(chunkTitle);
            paragraph.SpacingAfter = 0;

            var titleCell = new PdfPCell(paragraph);
            titleCell.BorderWidth = 0;
            titleCell.Padding = 0;
            titleCell.PaddingLeft = 3;
            titleCell.PaddingTop = 15;
            titleCell.PaddingBottom = -3;
            documentTable.AddCell(titleCell);

            cell.BorderWidth = 0.5f;
            cell.PaddingTop = 12;
            cell.PaddingBottom = 10;
            cell.PaddingLeft = 3;
            cell.PaddingRight = 3;

            documentTable.AddCell(cell);
        }
    }
}
