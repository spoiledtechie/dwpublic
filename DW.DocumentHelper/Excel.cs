﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DW.Library.Classes;
using DW.Library.DataModels;
using OfficeOpenXml;

namespace DW.DocumentHelper
{
    public class Excel
    {

        public static ExcelPackage CreateResultsSheet(string fileName, string sheetName, string title, string subtitle, List<ResultVM> results, List<ResultVM> prevResults, List<QuarterlyTiersVM> tiers)
        {
            FileInfo newFile = new FileInfo(fileName);

            ExcelPackage p = new ExcelPackage(newFile);


            var reportSheet = p.Workbook.Worksheets.Where(x => x.Name == sheetName).FirstOrDefault();
            if (reportSheet == null)
            {
                var template = p.Workbook.Worksheets.Where(x => x.Name == "Template").FirstOrDefault();

                p.Workbook.Worksheets.Add(sheetName, template);
                p.Workbook.Worksheets.MoveToStart(sheetName);
            }

            reportSheet = p.Workbook.Worksheets.Where(x => x.Name == sheetName).FirstOrDefault();
            int row = 3;

            reportSheet.Cells[1, 1].Value = title;
            reportSheet.Cells[2, 5].Value = subtitle;

            int rowIndex = 4;
            foreach (var result in results)
            {
                reportSheet.Cells[rowIndex, 1].Value = result.ClientId.ToString();
                reportSheet.Cells[rowIndex, 2].Value = result.ClientName.ToString();

                var initDate = DealerManager.GetDealerInitial(result.Dealer.Id).Result.MonthDateTime;
                string start = DateTimeFormatInfo.CurrentInfo?.GetAbbreviatedMonthName(initDate.Month) + "-" + initDate.ToString("yy");
                //cell = GetCell(worksheetPart, rowIndex, "C");
                reportSheet.Cells[rowIndex, 3].Value = start;
                reportSheet.Cells[rowIndex, 5].Value = result.Overall;
                reportSheet.Cells[rowIndex, 8].Value = result.FirstYear;
                reportSheet.Cells[rowIndex, 11].Value = result.InWar;
                reportSheet.Cells[rowIndex, 14].Value = result.OutWar;

                var prevResult = prevResults.FirstOrDefault(x => x.ClientId == result.ClientId) ?? new ResultVM();
                reportSheet.Cells[rowIndex, 17].Value = prevResult.Overall;
                reportSheet.Cells[rowIndex, 18].Value = prevResult.FirstYear;
                reportSheet.Cells[rowIndex, 19].Value = prevResult.InWar;
                reportSheet.Cells[rowIndex, 20].Value = prevResult.OutWar;


                rowIndex++;
            }

            rowIndex = 3;
            foreach (var qTier in tiers)
            {
                reportSheet.Cells[rowIndex, 22].Value = qTier.State + "- In Warranty";
                rowIndex++;
                reportSheet.Cells[rowIndex, 22].Value = "Tier 1";
                reportSheet.Cells[rowIndex, 23].Value = qTier.INWT1;
                rowIndex++;
                reportSheet.Cells[rowIndex, 22].Value = "Tier 2";
                reportSheet.Cells[rowIndex, 23].Value = qTier.INWT2;
                rowIndex++;
                reportSheet.Cells[rowIndex, 22].Value = "Out Warranty";
                rowIndex++;
                reportSheet.Cells[rowIndex, 22].Value = "Tier 1";
                reportSheet.Cells[rowIndex, 23].Value = qTier.OOWT1;
                rowIndex++;
                reportSheet.Cells[rowIndex, 22].Value = "Tier 2";
                reportSheet.Cells[rowIndex, 23].Value = qTier.OOWT2;
                rowIndex++;
            }

            // save updated info in results.xlsx
            p.Save();
            return p;
        }

        public static ExcelPackage CreateVisitsSheet(string fileName, string sheetName, string title, string subtitle, DealerVM dealer, DealerVM initDealer)
        {
            FileInfo newFile = new FileInfo(fileName);

            ExcelPackage p = new ExcelPackage(newFile);

            var reportSheet = p.Workbook.Worksheets.Where(x => x.Name == "Template").FirstOrDefault();
            reportSheet.Name = title;
            int row = 3;

            reportSheet.Cells[2, 1].Value = title;

            var dealerPrevious = initDealer.InitialReport;
            var regionalPrevious = initDealer.InitialReport;
            //gotta multiply by this number in order for it to be viewed in excel properly.
            decimal mulitplier = (decimal).01;
            reportSheet.Cells[5, 1].Value = "START - " + dealerPrevious.MonthDateTime.ToString("MMMM") + " " + dealerPrevious.MonthDateTime.Year;
            reportSheet.Cells[8, 2].Value = (dealerPrevious.OverallRetention * mulitplier);
            reportSheet.Cells[8, 4].Value = (dealerPrevious.Visits4Plus * mulitplier);
            reportSheet.Cells[8, 6].Value = (dealerPrevious.Visits3 * mulitplier);
            reportSheet.Cells[8, 8].Value = (dealerPrevious.Visits2 * mulitplier);
            reportSheet.Cells[8, 10].Value = (dealerPrevious.Visits1 * mulitplier);
            reportSheet.Cells[8, 12].Value = (dealerPrevious.Visits0 * mulitplier);

            reportSheet.Cells[9, 2].Value = (regionalPrevious.OverallRetention * mulitplier);
            reportSheet.Cells[9, 4].Value = (regionalPrevious.Visits4Plus * mulitplier);
            reportSheet.Cells[9, 6].Value = (regionalPrevious.Visits3 * mulitplier);
            reportSheet.Cells[9, 8].Value = (regionalPrevious.Visits2 * mulitplier);
            reportSheet.Cells[9, 10].Value = (regionalPrevious.Visits1 * mulitplier);
            reportSheet.Cells[9, 12].Value = (regionalPrevious.Visits0 * mulitplier);

            var dealerCurr = dealer.ReportD;
            var regionalCurr = dealer.ReportR;

            reportSheet.Cells[11, 1].Value = "CURRENT - " + dealerCurr.MonthDateTime.ToString("MMMM") + " " + dealerCurr.MonthDateTime.Year;
            reportSheet.Cells[14, 2].Value = (dealerCurr.OverallRetention * mulitplier);
            reportSheet.Cells[14, 4].Value = (dealerCurr.Visits4Plus * mulitplier);
            reportSheet.Cells[14, 6].Value = (dealerCurr.Visits3 * mulitplier);
            reportSheet.Cells[14, 8].Value = (dealerCurr.Visits2 * mulitplier);
            reportSheet.Cells[14, 10].Value = (dealerCurr.Visits1 * mulitplier);
            reportSheet.Cells[14, 12].Value = (dealerCurr.Visits0 * mulitplier);

            reportSheet.Cells[15, 2].Value = (regionalCurr.OverallRetention * mulitplier);
            reportSheet.Cells[15, 4].Value = (regionalCurr.Visits4Plus * mulitplier);
            reportSheet.Cells[15, 6].Value = (regionalCurr.Visits3 * mulitplier);
            reportSheet.Cells[15, 8].Value = (regionalCurr.Visits2 * mulitplier);
            reportSheet.Cells[15, 10].Value = (regionalCurr.Visits1 * mulitplier);
            reportSheet.Cells[15, 12].Value = (regionalCurr.Visits0 * mulitplier);


            return p;
        }

    }
}
