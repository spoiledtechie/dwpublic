﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using DW.DocumentHelper;
using DW.Library.Classes;
using DW.Library.DataModels;
using Spire.Xls;

namespace DW.Controllers
{
    public class DWController : Controller
    {
        // GET: DW
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult FetchMonthsYears()
        {
            List<SelectListItem> years = new List<SelectListItem>();
            for (int i = DateTime.Now.Year - 10; i <= DateTime.Now.Year + 10; i++)
            {
                years.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }
            List<SelectListItem> months = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                months.Add(new SelectListItem
                {
                    Text = $"{i} {DateTimeFormatInfo.CurrentInfo?.GetAbbreviatedMonthName(i)}",
                    Value = i.ToString()
                });
            }
            return Json(new { years, months, selectedYear = DateTime.Now.Year, selectedMonth = "" }, JsonRequestBehavior.AllowGet);
        }

        #region full report results
        public ActionResult FullReportResults(int? m, int? y)
        {
            if (m.HasValue)
                ViewBag.SelectedMonth = m.Value;
            if (y.HasValue)
                ViewBag.SelectedYear = y.Value;
            return View();
        }

        public JsonResult FetchFullReportResults(int m, int y)
        {
            var reports = DealerManager.GetFullReport(m, y);
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchColorsFullReportResults()
        {
            return Json(TableHub.fullReportColors, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region results
        public ActionResult Results(int? m, int? y)
        {
            if (m.HasValue)
                ViewBag.SelectedMonth = m.Value;
            if (y.HasValue)
                ViewBag.SelectedYear = y.Value;
            return View();
        }

        public JsonResult FetchResults(int m, int y)
        {
            var results = ResultManager.GetResults(m, y);
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateResult(long id, string property, string value)
        {
            //save to db
            if (ResultManager.UpdateItem(id, property, value))
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchColorsResult()
        {
            return Json(TableHub.resultsColors, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ExportResults(int m, int y)
        {
            string handler = Guid.NewGuid().ToString();
            string monthYear = DateTimeFormatInfo.CurrentInfo?.GetAbbreviatedMonthName(m) + " " + y;

            string title = "2017 DEALERWING NISSAN SERVICE RETENTION PROGRAM";
            string subTitle = DateTimeFormatInfo.CurrentInfo?.GetMonthName(m) + " " + y;
            var results = ResultManager.GetResults(m, y);
            var prevResults = m > 1 ? ResultManager.GetResults(m - 1, y) : ResultManager.GetResults(12, y - 1);
            var tiers = QuarterlyTiersManager.GetQuarterlyTiers(m, y);

            string baseLocation = AppDomain.CurrentDomain.BaseDirectory;
            string appData = baseLocation + @"App_Data\results.xlsx";


            var package = DW.DocumentHelper.Excel.CreateResultsSheet(appData, monthYear, title, subTitle, results, prevResults, tiers);
            string fileName = "Results-" + monthYear + ".xlsx";
            string tempFileLocation = baseLocation + @"App_Data\" + fileName;
            //Results-Sept 2016.xlsx
            FileInfo fi = new FileInfo(tempFileLocation);
            package.SaveAs(fi);

            TempData[handler] = tempFileLocation;

            return Json(new { success = true, fileGuid = handler, fileName = fileName }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult ExportVisits(int m, int y, int id)
        {
            string handler = Guid.NewGuid().ToString();
            string monthYear = DateTimeFormatInfo.CurrentInfo?.GetAbbreviatedMonthName(m) + " " + y;

            var dealer = DealerManager.GetDealer(id, m, y);
            var initialDealer = DealerManager.GetDealerInitial(id);

            string subTitle = DateTimeFormatInfo.CurrentInfo?.GetMonthName(m) + " " + y;

            string baseLocation = AppDomain.CurrentDomain.BaseDirectory;
            string appData = baseLocation + @"App_Data\visits.xlsx";


            var package = DW.DocumentHelper.Excel.CreateVisitsSheet(appData, monthYear, dealer.Name, subTitle, dealer, initialDealer);


            string fileName = "Visits-" + monthYear + ".xlsx";
            string fileNamePdf = "Visits-" + monthYear + ".pdf";
            string tempFileLocation = baseLocation + @"App_Data\" + fileName;
            string tempFileLocationPdf = baseLocation + @"App_Data\" + fileNamePdf;
            //Results-Sept 2016.xlsx
            FileInfo fi = new FileInfo(tempFileLocation);
            package.SaveAs(fi);

            Workbook wb = new Workbook();
            wb.LoadFromFile(tempFileLocation, ExcelVersion.Version2010);
            wb.SaveToFile(tempFileLocationPdf, FileFormat.PDF);


            TempData[handler] = tempFileLocationPdf;

            return Json(new { success = true, fileGuid = handler, fileName = fileNamePdf }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult DownloadExcel(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                var bytes = System.IO.File.ReadAllBytes(TempData[fileGuid].ToString());
                return File(bytes, "application/vnd.ms-excel", fileName);
            }
            else
            {
                return new EmptyResult();
            }
        }

        [HttpGet]
        public JsonResult ExportPdf(int m, int y, int clientId)
        {
            var curDealer = DealerManager.GetDealer(clientId, m, y);
            var iniDealer = DealerManager.GetDealerInitial(clientId);
            StateVM state;
            if (curDealer.StateID.HasValue)
            {
                state = StateManager.GetState(curDealer.StateID.Value);
            }
            else
            {
                return Json(new { success = false, message= "Dealer State Not Set" }, JsonRequestBehavior.AllowGet);
            }

            string handler = Guid.NewGuid().ToString();
            var tiers = QuarterlyTiersManager.GetQuarterlyTiersForState(m, y, state.Abbreviation);
            bool b;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                b = PDF.GeneratePDF(memoryStream, curDealer, iniDealer, tiers);
                memoryStream.Position = 0;
                TempData[handler] = memoryStream.ToArray();
            }
            if (b)
            {
                return Json(new { success = true, fileGuid = handler, fileName = curDealer.Name + ".pdf" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult DownloadPDF(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/pdf", fileName);
            }
            else
            {
                return new EmptyResult();
            }
        }
        #endregion

        #region report
        public ActionResult Report(int? m, int? y)
        {
            if (m.HasValue)
                ViewBag.SelectedMonth = m.Value;
            if (y.HasValue)
                ViewBag.SelectedYear = y.Value;
            return View();
        }

        public JsonResult FetchReport(int m, int y)
        {
            var reports = ReportManager.GetReports(m, y);
            return Json(reports, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateReport(long id, string property, string value)
        {
            //save to db
            if (ReportManager.UpdateItem(id, property, value))
            {
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchColorsReport()
        {
            return Json(TableHub.reportsColors, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}