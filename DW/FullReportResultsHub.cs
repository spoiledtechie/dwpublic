﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;

namespace DW
{
    public class FullReportResultsHub : Hub
    {
        public static Dictionary<string, string> colors = new Dictionary<string, string>();
        
        /// <summary>
        /// update color
        /// </summary>
        public void SendColorPosition(string property, string color)
        {
            if (string.IsNullOrWhiteSpace(color))
            {
                colors.Remove(property);
            }
            else
            {
                colors[property] = color; // if doesn't exist it will be added
            }
            // Call the broadcastColor method to update clients.
            Clients.All.broadcastColor( property, color);
        }
    }
}