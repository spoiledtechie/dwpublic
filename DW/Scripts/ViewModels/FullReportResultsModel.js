﻿//const
var C_FULL_REPORT_RESULTS = {
    URL: {
        FETCH_COLORS: "",
        FETCH_FULL_REPORT_RESULTS: "",
        FETCH_MONTHS_YEARS: ""
    }
};

var Client = function (clientId, clientName, result, reportDealer, reportRegional) {
    var self = this;
    self.clientId = clientId;
    self.clientName = clientName;
    self.result = result;
    self.reportDealer = reportDealer;
    self.reportRegional = reportRegional;
}

function FullReportResultsViewModel() {
    var self = this;

    self.years = ko.observableArray([]);
    self.months = ko.observableArray([]);
    self.year = ko.observable();
    self.month = ko.observable();
    self.year.subscribe(function (newValue) {
        if (newValue && self.month())
            self.fetchRows(self.month(), newValue);
    });
    self.month.subscribe(function (newValue) {
        if (newValue && self.year())
            self.fetchRows(newValue, self.year());
        if (!newValue)
            self.clients.removeAll();
    });
    //random color
    self.color = '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6);
    //hub
    self.hub = $.connection.tableHub;
    self.hubId = ko.observable();
    //data rows
    self.clients = ko.observableArray([]);

    // for result part
    // Create a function that the hub can call to broadcast messages.
    self.hub.client.broadcastResultMessage = function (id, property, value, clientHubId) {
        //update the value here (for this row), if this page is not the page where the change has been done
        if ($.connection.hub.id != clientHubId) {
            var client = ko.utils.arrayFirst(self.clients(), function (r) {
                return r.result.id === id;
            });
            var result = client.result;
            //if this page has a row (visible) with this id then update it
            if (result) {
                vm.hubId(clientHubId); // this will prevent to save the same data again in db.
                if (property === C_RESULT.ENUM.FIRTS_YEAR) {
                    result.firstYear(value);
                } else if (property === C_RESULT.ENUM.IN_WAR) {
                    result.inWar(value);
                } else if (property === C_RESULT.ENUM.OUT_WAR) {
                    result.outWar(value);
                } else if (property === C_RESULT.ENUM.CUST_PAY_RO_VS_PRIOR_YR) {
                    result.custPayRo(value);
                } else if (property === C_RESULT.ENUM.CUST_PAY_MONEY_VS_PRIORT_YR) {
                    result.custPayMoney(value);
                } else if (property === C_RESULT.ENUM.ABC_NOW) {
                    result.abcNow(value);
                } else if (property === C_RESULT.ENUM.ZERO_VISIT_NOW) {
                    result.zeroVisitNow(value);
                } else if (property === C_RESULT.ENUM.TWO_VISIT_NOW) {
                    result.twoVisitNow(value);
                } else if (property === C_RESULT.ENUM.CROSS_NOW) {
                    result.crossNow(value);
                }
            }
        }
    };

    // for report part
    // Create a function that the hub can call to broadcast messages.
    self.hub.client.broadcastReportMessage = function (id, property, value, clientHubId) {
        //update the value here (for this row), if this page is not the page where the change has been done
        if ($.connection.hub.id != clientHubId) {
            var client = ko.utils.arrayFirst(self.clients(), function (r) {
                return r.reportDealer.id === id || r.reportRegional.id === id;
            });
            var report = client.reportDealer.id === id ? client.reportDealer : client.reportRegional;
            //if this page has a row (visible) with this id then update it
            if (report) {
                vm.hubId(clientHubId); // this will prevent to save the same data again in the db.
                if (property === C_REPORT.ENUM.OVERALL_RETENTION) {
                    report.overallRetention(value);
                } else if (property === C_REPORT.ENUM.VISITS4PLUS) {
                    report.visits4Plus(value);
                } else if (property === C_REPORT.ENUM.VISITS3) {
                    report.visits3(value);
                } else if (property === C_REPORT.ENUM.VISITS2) {
                    report.visits2(value);
                } else if (property === C_REPORT.ENUM.VISITS1) {
                    report.visits1(value);
                } else if (property === C_REPORT.ENUM.VISITS0) {
                    report.visits0(value);
                }
            }
        }
    };

    //update Overall column
    self.hub.client.broadcastOverallMessage = function (clientId, value, clientHubId) {
        if ($.connection.hub.id != clientHubId) {
            var client = ko.utils.arrayFirst(self.clients(), function (r) {
                return r.clientId === clientId;
            });
            if (client.result) {
                vm.hubId(clientHubId); // this will prevent to save the same data again in the db.
                client.result.overall(value);
            }
            //this line is not needed because the reportDealer.overallRetention is not visible on the page
            if (client.reportDealer) {
                client.reportDealer.overallRetention(value);
            }
        }
    };

    // hub function for color
    self.hub.client.broadcastFullReportColor = function (property, color) {
        if (color) {
            $("." + property).css("border", "2px solid " + color);
        } else {
            $("." + property).css("border", "");
        }
    };

    self.init = function (month, year) {
        $.get(C_FULL_REPORT_RESULTS.URL.FETCH_MONTHS_YEARS, function (result) {
            if (result) {
                if (result.years && result.months) {
                    result.years.forEach(function (y) {
                        self.years.push(y.Value);
                    });
                    result.months.forEach(function (m) {
                        self.months.push(new MonthObj(m.Text, m.Value));
                    });
                    if (year) {
                        self.year(year);
                    } else {
                        self.year(result.selectedYear);
                    }
                    if (month) {
                        self.month(month);
                    } else {
                        self.month(result.selectedMonth);
                    }
                }
            }
        });
        //start signalR hubs
        $.connection.hub.start().done(function () {
            self.hubId($.connection.hub.id);
        });
    };

    self.fetchRows = function (month, year) {
        $.get(C_FULL_REPORT_RESULTS.URL.FETCH_FULL_REPORT_RESULTS, { m: month, y: year }, function (entities) {
            self.clients.removeAll();
            //load rows
            entities.forEach(function (c) {
                self.clients.push(
                    new Client(c.Id, c.Name,
                        new Result(c.Result),
                        new Report(c.ReportD),
                        new Report(c.ReportR)
                    ));
            });
            //make Enter key behave like Tab key
            Base.enterKeyPress();
            //change border color for row on cell focus
            Base.cellFocusChanged(self.color, self.hub.server.sendFullReportColorPosition);
        });
    };

    self.exportResults = function () {
        if (self.month() && self.year()) {
            $.get(C_RESULT.URL.EXPORT_RESULTS, { m: self.month(), y: self.year() }, function (result) {
                if (result.success) {
                    window.location = C_RESULT.URL.DOWNLOAD_EXCEL + '?fileGuid=' + result.fileGuid + '&filename=' + result.fileName;
                } else {
                    alert("An error has occurred!");
                }
            }
            );
        } else {
            alert("Please select a month and a year!");
        }
    };


    self.fetchColors = function () {
        $.get(C_FULL_REPORT_RESULTS.URL.FETCH_COLORS, function (colors) {
            $.each(colors, function (key, value) {
                $("." + key).css("border", "2px solid " + value);
            });
        });
    }

    self.exportPDF = function (id) {
        if (self.month() && self.year()) {
            Base.exportPDF(self.month(), self.year(), id);
        } else {
            alert("Please select a month and a year!");
        }
    }
    self.exportReports = function (id ) {
        if (self.month() && self.year()) {
            $.get(C_REPORT.URL.EXPORT_REPORTS, {id:id , m: self.month(), y: self.year() }, function (result) {
                if (result.success) {
                    window.location = C_RESULT.URL.DOWNLOAD_EXCEL + '?fileGuid=' + result.fileGuid + '&filename=' + result.fileName;
                } else {
                    alert("An error has occurred!");
                }
            }
            );
        } else {
            alert("Please select a month and a year!");
        }
    };
}