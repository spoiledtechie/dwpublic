﻿//const
var C_RESULT = {
    URL: {
        UPDATE_RESULT: "",
        FETCH_COLORS: "",
        FETCH_RESULTS: "",
        FETCH_MONTHS_YEARS: "",
        EXPORT_RESULTS: "",
        DOWNLOAD_EXCEL: ""
    },
    ENUM: {
        OVERALL: "",
        FIRTS_YEAR: "",
        IN_WAR: "",
        OUT_WAR: "",
        CUST_PAY_RO_VS_PRIOR_YR: "",
        CUST_PAY_MONEY_VS_PRIORT_YR: "",
        ABC_NOW: "",
        ZERO_VISIT_NOW: "",
        TWO_VISIT_NOW: "",
        CROSS_NOW: ""
    }
};

//notify other clients
ko.extenders.resultValueChange = function (target, option) {
    target.subscribe(function (newValue) {
        //save to db, (if this is the page where the change was done)
        if ($.connection.hub.id == vm.hubId()) { // prevent multiple save to db
            $.post(C_RESULT.URL.UPDATE_RESULT, { id: option.id, property: option.property, value: newValue }, function (data) {
                if (data.success) {
                    //notify about the change
                    if (option.property == C_RESULT.ENUM.OVERALL) {
                        vm.hub.server.sentOverallUpdate(option.clientId, newValue, $.connection.hub.id);
                    } else {
                        vm.hub.server.sendResultUpdate(option.id, option.property, newValue, $.connection.hub.id);
                    }
                }
            });
        }
        //reset hubId
        vm.hubId($.connection.hub.id);
    });
    return target;
};

//row object
var Result = function (result){
    var self = this;
    self.id = result.Id;
    self.clientId = result.ClientId;
    self.clientName = result.ClientName;
    self.overall = ko.observable(result.Overall).extend({ resultValueChange: { property: C_RESULT.ENUM.OVERALL, id: result.Id, clientId: result.ClientId } });
    self.firstYear = ko.observable(result.FirstYear).extend({ resultValueChange: { property: C_RESULT.ENUM.FIRTS_YEAR, id: result.Id } });
    self.inWar = ko.observable(result.InWar).extend({ resultValueChange: { property: C_RESULT.ENUM.IN_WAR, id: result.Id } });
    self.outWar = ko.observable(result.OutWar).extend({ resultValueChange: { property: C_RESULT.ENUM.OUT_WAR, id: result.Id } });
    self.custPayRo = ko.observable(result.CustPayRoVsPriorYr).extend({ resultValueChange: { property: C_RESULT.ENUM.CUST_PAY_RO_VS_PRIOR_YR, id: result.Id } });
    self.custPayMoney = ko.observable(result.CustPayMoneyVsPriortYr).extend({ resultValueChange: { property: C_RESULT.ENUM.CUST_PAY_MONEY_VS_PRIORT_YR, id: result.Id } });
    self.abcNow = ko.observable(result.AbcNow).extend({ resultValueChange: { property: C_RESULT.ENUM.ABC_NOW, id: result.Id } });
    self.zeroVisitNow = ko.observable(result.ZeroVisitNow).extend({ resultValueChange: { property: C_RESULT.ENUM.ZERO_VISIT_NOW, id: result.Id } });
    self.twoVisitNow = ko.observable(result.TwoVisitNow).extend({ resultValueChange: { property: C_RESULT.ENUM.TWO_VISIT_NOW, id: result.Id } });
    self.crossNow = ko.observable(result.CrossNow).extend({ resultValueChange: { property: C_RESULT.ENUM.CROSS_NOW, id: result.Id } });
}

function ResultListViewModel() {
    var self = this;

    self.years = ko.observableArray([]);
    self.months = ko.observableArray([]);
    self.year = ko.observable();
    self.month = ko.observable();
    self.year.subscribe(function(newValue) {
        if (newValue && self.month())
            self.fetchRows(self.month(), newValue);
    });
    self.month.subscribe(function (newValue) {
        if (newValue && self.year())
            self.fetchRows(newValue, self.year());
        if (!newValue)
            self.results.removeAll();
    });

    //random color
    self.color = '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6);
    //properties for data
    self.hub = $.connection.tableHub;
    self.hubId = ko.observable(); //hub if for the current page or the last that has done a modification
    self.results = ko.observableArray([]);

    //hub function for data
    // Create a function that the hub can call to broadcast messages.
    self.hub.client.broadcastResultMessage = function (id, property, value, clientHubId) {
        //update the value here (for this row), if this page is not the page where the change has been done
        if ($.connection.hub.id != clientHubId) {
            var result = ko.utils.arrayFirst(self.results(), function (r) {
                return r.id === id;
            });
            //if this page has a row (visible) with this id then update it
            if (result) {
                vm.hubId(clientHubId); // this will prevent to save the same data again in the db.
                if (property === C_RESULT.ENUM.FIRTS_YEAR) {
                    result.firstYear(value);
                } else if (property === C_RESULT.ENUM.IN_WAR) {
                    result.inWar(value);
                } else if (property === C_RESULT.ENUM.OUT_WAR) {
                    result.outWar(value);
                } else if (property === C_RESULT.ENUM.CUST_PAY_RO_VS_PRIOR_YR) {
                    result.custPayRo(value);
                } else if (property === C_RESULT.ENUM.CUST_PAY_MONEY_VS_PRIORT_YR) {
                    result.custPayMoney(value);
                } else if (property === C_RESULT.ENUM.ABC_NOW) {
                    result.abcNow(value);
                } else if (property === C_RESULT.ENUM.ZERO_VISIT_NOW) {
                    result.zeroVisitNow(value);
                } else if (property === C_RESULT.ENUM.TWO_VISIT_NOW) {
                    result.twoVisitNow(value);
                } else if (property === C_RESULT.ENUM.CROSS_NOW) {
                    result.crossNow(value);
                }
            }
        }
    };

    //update Overall column
    self.hub.client.broadcastOverallMessage = function (clientId, value, clientHubId) {
        if ($.connection.hub.id != clientHubId) {
            var result = ko.utils.arrayFirst(self.results(), function (r) {
                return r.clientId === clientId;
            });
            if (result) {
                vm.hubId(clientHubId); // this will prevent to save the same data again in the db.
                result.overall(value);
            }
        }
    };

    // hub function for color
    self.hub.client.broadcastResultColor = function (property, color) {
        if (color) {
            $("#" + property).css("border", "2px solid " + color);
        } else {
            $("#" + property).css("border", "");
        }
    };

    self.init = function (month, year) {
        $.get(C_RESULT.URL.FETCH_MONTHS_YEARS, function (result) {
            if (result) {
                if (result.years && result.months) {
                    result.years.forEach(function(y) {
                        self.years.push(y.Value);
                    });
                    result.months.forEach(function (m) {
                        self.months.push(new MonthObj(m.Text, m.Value));
                    });
                    if (year) {
                        self.year(year);
                    } else {
                        self.year(result.selectedYear);
                    }
                    if (month) {
                        self.month(month);
                    } else {
                        self.month(result.selectedMonth);
                    }
                }
            }
        });

        //start signalR hub
        $.connection.hub.start().done(function (){
            self.hubId($.connection.hub.id);
        });
    };

    self.fetchRows = function(month, year) {
        $.get(C_RESULT.URL.FETCH_RESULTS, { m: month, y: year }, function(entities) {
            self.results.removeAll();
            //load rows
            entities.forEach(function (e) {
                self.results.push(new Result(e));
            });
            //make Enter key behave like Tab key
            Base.enterKeyPress();
            //change border color for row on cell focus
            Base.cellFocusChanged(self.color, self.hub.server.sendResultColorPosition);
        });
    };

    self.fetchColors = function() {
        $.get(C_RESULT.URL.FETCH_COLORS, function(colors) {
            $.each(colors, function(key, value) {
                $("#" + key).css("border", "2px solid " + value);
            });
        });
    };

    self.exportResults = function() {
        if (self.month() && self.year()) {
            $.get(C_RESULT.URL.EXPORT_RESULTS, { m: self.month(), y: self.year() }, function(result) {
                    if (result.success) {
                        window.location = C_RESULT.URL.DOWNLOAD_EXCEL + '?fileGuid=' + result.fileGuid + '&filename=' + result.fileName;
                    } else {
                        alert("An error has occurred!");
                    }
                }
            );
        } else {
            alert("Please select a month and a year!");
        }
    };

    self.exportPDF = function (id) {
        if (self.month() && self.year()) {
            Base.exportPDF(self.month(), self.year(), id);
        } else {
            alert("Please select a month and a year!");
        }
    }
}