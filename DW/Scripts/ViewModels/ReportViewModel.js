﻿//const
var C_REPORT = {
    URL: {
        UPDATE_RESULT: "",
        FETCH_COLORS: "",
        FETCH_REPORT: "",
        FETCH_MONTHS_YEARS: ""
    },
    ENUM: {
        DEALER: "",
        REGIONAL: "",
        DEALER_STRING: "",
        REGIONAL_STRING: "",
        OVERALL_RETENTION: "",
        VISITS4PLUS: "",
        VISITS3: "",
        VISITS2: "",
        VISITS1: "",
        VISITS0: ""
    }
};

//notify other clients
ko.extenders.reportValueChange = function (target, option) {
    target.subscribe(function (newValue) {
        //save to db, (if this is the page where the change was done)
        if ($.connection.hub.id == vm.hubId()) { // prevent multiple save to db
            $.post(C_REPORT.URL.UPDATE_RESULT, { id: option.id, property: option.property, value: newValue }, function (data) {
                if (data.success) {
                    //notify about the change
                    if (option.property == C_REPORT.ENUM.OVERALL_RETENTION && option.courtesy == C_REPORT.ENUM.DEALER) {
                        vm.hub.server.sentOverallUpdate(option.clientId, newValue, $.connection.hub.id);
                    } else {
                        vm.hub.server.sendReportUpdate(option.id, option.property, newValue, $.connection.hub.id);
                    }
                }
            });
        }
        //reset hubId
        vm.hubId($.connection.hub.id);
    });
    return target;
};

//row object
var Report = function (report){
    var self = this;
    self.id = report.Id;
    self.clientId = report.ClientId;
    self.clientName = report.ClientName;

    self.courtesy = ko.observable();
    self.courtesyText = ko.computed({
        read: function () {
            var displayValue = "";
            if (self.courtesy() == C_REPORT.ENUM.DEALER) {
                displayValue = C_REPORT.ENUM.DEALER_STRING;
            } else if (self.courtesy() == C_REPORT.ENUM.REGIONAL) {
                displayValue = C_REPORT.ENUM.REGIONAL_STRING;
            }
            return displayValue;
        }
    });
    self.courtesy(report.Courtesy); // intial value

    self.overallRetention = ko.observable(report.OverallRetention).extend({ reportValueChange: { property: C_REPORT.ENUM.OVERALL_RETENTION, id: report.Id, clientId: report.ClientId, courtesy: report.Courtesy } });
    self.visits4Plus = ko.observable(report.Visits4Plus).extend({ reportValueChange: { property: C_REPORT.ENUM.VISITS4PLUS, id: report.Id } });
    self.visits3 = ko.observable(report.Visits3).extend({ reportValueChange: { property: C_REPORT.ENUM.VISITS3, id: report.Id } });
    self.visits2 = ko.observable(report.Visits2).extend({ reportValueChange: { property: C_REPORT.ENUM.VISITS2, id: report.Id } });
    self.visits1 = ko.observable(report.Visits1).extend({ reportValueChange: { property: C_REPORT.ENUM.VISITS1, id: report.Id } });
    self.visits0 = ko.observable(report.Visits0).extend({ reportValueChange: { property: C_REPORT.ENUM.VISITS0, id: report.Id } });
}

function ReportListViewModel() {
    var self = this;

    self.years = ko.observableArray([]);
    self.months = ko.observableArray([]);
    self.year = ko.observable();
    self.month = ko.observable();
    self.year.subscribe(function (newValue) {
        if (newValue && self.month())
            self.fetchRows(self.month(), newValue);
    });
    self.month.subscribe(function (newValue) {
        if (newValue && self.year())
            self.fetchRows(newValue, self.year());
        if (!newValue)
            self.reports.removeAll();
    });
    //random color
    self.color = '#' + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6);
    //properties for data
    self.hub = $.connection.tableHub;
    self.hubId = ko.observable(); //hub if for the current page or the last that has done a modification
    self.reports = ko.observableArray([]);
    // Create a function that the hub can call to broadcast messages.
    self.hub.client.broadcastReportMessage = function (id, property, value, clientHubId) {
        //update the value here (for this row), if this page is not the page where the change has been done
        if ($.connection.hub.id != clientHubId) {
            var report = ko.utils.arrayFirst(self.reports(), function (r) {
                return r.id === id;
            });
            //if this page has a row (visible) with this id then update it
            if (report) {
                vm.hubId(clientHubId); // this will prevent to save the same data again in the db.
                if (property === C_REPORT.ENUM.OVERALL_RETENTION) {
                    report.overallRetention(value);
                } else if (property === C_REPORT.ENUM.VISITS4PLUS) {
                    report.visits4Plus(value);
                } else if (property === C_REPORT.ENUM.VISITS3) {
                    report.visits3(value);
                } else if (property === C_REPORT.ENUM.VISITS2) {
                    report.visits2(value);
                } else if (property === C_REPORT.ENUM.VISITS1) {
                    report.visits1(value);
                } else if (property === C_REPORT.ENUM.VISITS0) {
                    report.visits0(value);
                }
            }
        }
    };

    //update Overall column
    self.hub.client.broadcastOverallMessage = function (clientId, value, clientHubId) {
        if ($.connection.hub.id != clientHubId) {
            var result = ko.utils.arrayFirst(self.reports(), function (r) {
                return r.clientId === clientId && r.courtesy() === C_REPORT.ENUM.DEALER;
            });
            if (result) {
                vm.hubId(clientHubId); // this will prevent to save the same data again in the db.
                result.overallRetention(value);
            }
        }
    };

    // hub function for color
    self.hub.client.broadcastReportColor = function (property, color) {
        if (color) {
            $("." + property).css("border", "2px solid " + color);
        } else {
            $("." + property).css("border", "");
        }
    };
    
    self.init = function (month, year) {
        $.get(C_REPORT.URL.FETCH_MONTHS_YEARS, function (result) {
            if (result) {
                if (result.years && result.months) {
                    result.years.forEach(function (y) {
                        self.years.push(y.Value);
                    });
                    result.months.forEach(function (m) {
                        self.months.push(new MonthObj(m.Text, m.Value));
                    });
                    if (year) {
                        self.year(year);
                    } else {
                        self.year(result.selectedYear);
                    }
                    if (month) {
                        self.month(month);
                    } else {
                        self.month(result.selectedMonth);
                    }
                }
            }
        });
        //start signalR hub
        $.connection.hub.start().done(function () {
            self.hubId($.connection.hub.id);
        });
    };

    self.fetchRows = function (month, year) {
        $.get(C_REPORT.URL.FETCH_REPORT, { m: month, y: year }, function (entities) {
            self.reports.removeAll();
            //load rows
            entities.forEach(function (e) {
                self.reports.push(new Report(e));
            });
            //make Enter key behave like Tab key
            Base.enterKeyPress();
            //change border color for row on cell focus
            Base.cellFocusChanged(self.color, self.hub.server.sendReportColorPosition);
        });
    };

    self.exportReports = function (id) {
        if (self.month() && self.year()) {
            $.get(C_REPORT.URL.EXPORT_REPORTS, { id: id, m: self.month(), y: self.year() }, function (result) {
                if (result.success) {
                    window.location = C_REPORT.URL.DOWNLOAD_EXCEL + '?fileGuid=' + result.fileGuid + '&filename=' + result.fileName;
                } else {
                    alert("An error has occurred!");
                }
            }
            );
        } else {
            alert("Please select a month and a year!");
        }
    };

    self.fetchColors = function() {
        $.get(C_REPORT.URL.FETCH_COLORS, function (colors) {
            $.each(colors, function (key, value) {
                $("#" + key).css("border", "2px solid " + value);
            });
        });
    }
}