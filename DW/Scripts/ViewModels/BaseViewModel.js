﻿var C_BASE= {
    URL: {
        EXPORT_PDF: "",
        DOWNLOAD_PDF: ""
    }
}

var Base = (function () {
    var self = this;
    self.enterKeyPress = function() {
        //enter -> focus next
        $(".input-cell").on("focus", function() { $(this).select(); });
        $('.input-cell').on("keypress", function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $('input.input-cell');
                var index;
                if (e.shiftKey) {
                    index = $canfocus.index(document.activeElement) - 1;
                    if (index < 0) index = $canfocus.length - 1;
                } else {
                    index = $canfocus.index(document.activeElement) + 1;
                    if (index >= $canfocus.length) index = 0;
                }
                $canfocus.eq(index).focus();
            }
        });
    };
    self.cellFocusChanged = function(color, serverFunction) {
        //focus has changed -> change color selection
        $('.input-cell').on("focus", function(e) {
            var property = $(this).data('property');
            serverFunction(property, color);
        });
        $('.input-cell').on("blur", function(e) {
            var property = $(this).data('property');
            serverFunction(property, "");
        });
    };

    self.exportPDF = function (m, y, id) {
        $.get(C_BASE.URL.EXPORT_PDF, { m: m, y: y, clientId: id }, function (result) {
            if (result.success) {
                window.location = C_BASE.URL.DOWNLOAD_PDF + '?fileGuid=' + result.fileGuid + '&filename=' + result.fileName;
            } else {
                alert("An error has occurred! "+ result.message);
            }
        });
    }

    return {
        enterKeyPress: enterKeyPress,
        cellFocusChanged: cellFocusChanged,
        exportPDF: exportPDF
    };
})();

var MonthObj = function (name, value) {
    var self = this;
    self.monthName = name;
    self.monthValue = value;
}

//format percentage
ko.bindingHandlers.percentNumber = {
    init: function (element, valueAccessor) {
        var options = {
            mDec: 2,
            aSep: '',
            aSign: '%',
            pSign: 's',
            vMax: 999.99,
            wEmpty: 'zero'
        };
        $(element).autoNumeric(options);

        ko.utils.registerEventHandler(element, 'focusout', function () {
            var observable = valueAccessor();
            // this get the value of the input without the formating ( percentage )
            value = $(element).autoNumeric('get');
            observable(isNaN(value) ? 0 : value);
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).autoNumeric('set', value);
    }
};

ko.bindingHandlers.number = {
    init: function (element, valueAccessor) {
        var options = {
            mDec: 0,
            wEmpty: 'zero'
        };
        $(element).autoNumeric(options);

        ko.utils.registerEventHandler(element, 'focusout', function () {
            var observable = valueAccessor();
            // this get the value of the input without the formating ( percentage )
            value = $(element).autoNumeric('get');
            observable(isNaN(value) ? 0 : value);
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).autoNumeric('set', value);
    }
};