﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;

namespace DW
{
    public class TableHub : Hub
    {
        public static Dictionary<string, string> resultsColors = new Dictionary<string, string>();
        public static Dictionary<string, string> reportsColors = new Dictionary<string, string>();
        public static Dictionary<string, string> fullReportColors = new Dictionary<string, string>();

        /// <summary>
        /// sends the new value to the other clients
        /// </summary>
        /// <param name="id"> the result id</param>
        /// <param name="property"> a property (FirstYear, InWar or OutWar)</param>
        /// <param name="value">the new value for the property</param>
        public void SendResultUpdate(long id, string property, string value, string clientHubId)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastResultMessage(id, property, value, clientHubId);
        }

        /// <summary>
        /// update color
        /// </summary>
        public void SendResultColorPosition(string property, string color)
        {
            if (string.IsNullOrWhiteSpace(color))
            {
                resultsColors.Remove(property);
            }
            else
            {
                resultsColors[property] = color; // if doesn't exist it will be added
            }
            // Call the broadcastColor method to update clients.
            Clients.All.broadcastResultColor( property, color);
        }


        /// <summary>
        /// sends the new value to the other clients
        /// </summary>
        /// <param name="id"> the result id</param>
        /// <param name="property"> a property (Visits4Plus, Visits3, Visits2, Visits1 or Visits0)</param>
        /// <param name="value">the new value for the property</param>
        public void SendReportUpdate(long id, string property, string value, string clientHubId)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastReportMessage(id, property, value, clientHubId);
        }

        /// <summary>
        /// update color
        /// </summary>
        public void SendReportColorPosition(string property, string color)
        {
            if (string.IsNullOrWhiteSpace(color))
            {
                reportsColors.Remove(property);
            }
            else
            {
                reportsColors[property] = color; // if doesn't exist it will be added
            }
            // Call the broadcastColor method to update clients.
            Clients.All.broadcastReportColor(property, color);
        }

        /// <summary>
        /// update color
        /// </summary>
        public void SendFullReportColorPosition(string property, string color)
        {
            if (string.IsNullOrWhiteSpace(color))
            {
                fullReportColors.Remove(property);
            }
            else
            {
                fullReportColors[property] = color; // if doesn't exist it will be added
            }
            // Call the broadcastColor method to update clients.
            Clients.All.broadcastFullReportColor(property, color);
        }

        // this is used in both Report and Results tables for overall column
        public void SentOverallUpdate(long clientId, string value, string clientHubId)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastOverallMessage(clientId, value, clientHubId);
        }
    }
}