﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;

namespace DW
{
    public class ResultHub : Hub
    {
        public static Dictionary<string, string> colors = new Dictionary<string, string>(); 

        /// <summary>
        /// sends the new value to the other clients
        /// </summary>
        /// <param name="id"> the result id</param>
        /// <param name="property"> a property ( Overall, FirstYear, InWar or OutWar)</param>
        /// <param name="value">the new value for the property</param>
        public void SendUpdate(long id, string property, string value, string clientHubId)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(id, property, value, clientHubId);
        }

        /// <summary>
        /// update color
        /// </summary>
        public void SendColorPosition(string property, string color)
        {
            if (string.IsNullOrWhiteSpace(color))
            {
                colors.Remove(property);
            }
            else
            {
                colors[property] = color; // if doesn't exist it will be added
            }
            // Call the broadcastColor method to update clients.
            Clients.All.broadcastColor(property, color);
        }
    }
}