﻿using System;
using System.Web.Script.Serialization;

namespace DW.Library.Classes
{
    public class ReportVM
    {
        public long Id { get; set; }
        [ScriptIgnore]
        public DealerVM Dealer { get; set; }
        public DateTime MonthDateTime { get; set; }
        //one char string ( D or R )
        public string Courtesy { get; set; }
        public decimal OverallRetention { get; set; }
        public decimal Visits4Plus { get; set; }
        public decimal Visits3 { get; set; }
        public decimal Visits2 { get; set; }
        public decimal Visits1 { get; set; }
        public decimal Visits0 { get; set; }

        public string ClientName => Dealer != null ? Dealer.Name : string.Empty;
        public long ClientId => Dealer != null ? Dealer.Id : -1;
    }
}
