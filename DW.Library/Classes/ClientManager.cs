﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DW.Library.Constant;
using DW.Library.DataModels;

namespace DW.Library.Classes
{
    public class ClientManager
    {
        public static List<ClientVM> GetFullReport(int month, int year)
        {
            List<ClientVM> clientsList = new List<ClientVM>();
            var selectedDate = new DateTime(year, month, 1);
            using (var context = new Context())
            {
                var clients = context.Clients.Include("Reports").Include("Results").ToList();
                foreach (var client in clients)
                {
                    var clientVM = new ClientVM();
                    clientVM.Id = client.Id;
                    clientVM.Name = client.Name;
                    var reportD = client.Reports.FirstOrDefault(x => x.MonthDateTime == selectedDate && x.Courtesy == CCourtesy.Dealer);
                    if (reportD == null)
                    {
                        reportD = new Report();
                        reportD.MonthDateTime = selectedDate;
                        reportD.Client = client;
                        reportD.Courtesy = CCourtesy.Dealer;
                        context.Reports.Add(reportD);
                        context.SaveChanges();  // this is needed here for report id
                    }
                    clientVM.ReportD = new ReportVM()
                    {
                        Id = reportD.Id,
                        Client = clientVM,
                        MonthDateTime = reportD.MonthDateTime,
                        Courtesy = reportD.Courtesy,
                        OverallRetention = reportD.OverallRetention,
                        Visits4Plus = reportD.Visits4Plus,
                        Visits3 = reportD.Visits3,
                        Visits2 = reportD.Visits2,
                        Visits1 = reportD.Visits1,
                        Visits0 = reportD.Visits0,
                    };

                    var reportR = client.Reports.FirstOrDefault(x => x.MonthDateTime == selectedDate && x.Courtesy == CCourtesy.Regional);
                    if (reportR == null)
                    {
                        reportR = new Report();
                        reportR.MonthDateTime = selectedDate;
                        reportR.Client = client;
                        reportR.Courtesy = CCourtesy.Regional;
                        context.Reports.Add(reportR);
                        context.SaveChanges();  // this is needed here for report id
                    }
                    clientVM.ReportR = new ReportVM()
                    {
                        Id = reportR.Id,
                        Client = clientVM,
                        MonthDateTime = reportR.MonthDateTime,
                        Courtesy = reportR.Courtesy,
                        OverallRetention = reportR.OverallRetention,
                        Visits4Plus = reportR.Visits4Plus,
                        Visits3 = reportR.Visits3,
                        Visits2 = reportR.Visits2,
                        Visits1 = reportR.Visits1,
                        Visits0 = reportR.Visits0,
                    };

                    var result = client.Results.FirstOrDefault(x => x.MonthDateTime == selectedDate);
                    if (result == null)
                    {
                        result = new Result();
                        result.MonthDateTime = selectedDate;
                        result.Client = client;
                        context.Results.Add(result);
                        context.SaveChanges();  // this is needed here for result id
                    }
                    clientVM.Result = new ResultVM
                    {
                        Id = result.Id,
                        Client = clientVM,
                        MonthDateTime = result.MonthDateTime,
                        Overall = reportD.OverallRetention,
                        FirstYear = result.FirstYr,
                        InWar = result.InWar,
                        OutWar = result.OutWar,
                    };
                    clientsList.Add(clientVM);
                }
            }
            return clientsList;
        }
    }
}
