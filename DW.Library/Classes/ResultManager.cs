﻿using System;
using System.Collections.Generic;
using System.Linq;
using DW.Library.Constant;
using DW.Library.DataModels;

namespace DW.Library.Classes
{
    public class ResultManager
    {
        /// <summary>
        /// get all rows for this month (if a client doesn't have a row for this month one row will be created)
        /// </summary>
        public static List<ResultVM> GetResults(int month, int year)
        {
            List<ResultVM> resultsList = new List<ResultVM>();
            var selectedDate = new DateTime(year, month, 1);
            using (var context = new Context())
            {
                var dealers = context.Dealers.Include("Results").Include("Reports").ToList();
                foreach (var dealer in dealers)
                {
                    var result = dealer.Results.FirstOrDefault(x => x.MonthDateTime == selectedDate);
                    if (result == null)
                    {
                        result = new Result();
                        result.MonthDateTime = selectedDate;
                        result.Dealer = dealer;
                        context.Results.Add(result);
                        context.SaveChanges(); // this is needed here for result id
                    }

                    // get overall from report table
                    var report = dealer.Reports.FirstOrDefault(x => x.MonthDateTime == selectedDate && x.Courtesy == CCourtesy.Dealer);
                    decimal reportDecimal;
                    if (report == null)
                    {
                        var reportD = new Report();
                        reportD.MonthDateTime = selectedDate;
                        reportD.Dealer = dealer;
                        reportD.Courtesy = CCourtesy.Dealer;
                        context.Reports.Add(reportD);
                        context.SaveChanges();

                        reportDecimal = reportD.OverallRetention;
                    }
                    else
                    {
                        reportDecimal = report.OverallRetention;
                    }

                    var r = new ResultVM();
                    r.Id = result.Id;

                    r.Dealer = new DealerVM();
                    r.Dealer.Id = result.Dealer.DealerID;
                    r.Dealer.Name = result.Dealer.DealerShipName;
                    r.MonthDateTime = result.MonthDateTime;

                    r.Overall = reportDecimal;
                    r.FirstYear = result.FirstYr;
                    r.InWar = result.InWar;
                    r.OutWar = result.OutWar;
                    r.CustPayRoVsPriorYr = result.CustPayRoVsPriorYr;
                    r.CustPayMoneyVsPriortYr = result.CustPayMoneyVsPriortYr;

                    r.AbcNow = result.AbcNow;
                    r.ZeroVisitNow = result.ZeroVisitNow;
                    r.TwoVisitNow = result.TwoVisitNow;
                    r.CrossNow = result.CrossNow;

                    resultsList.Add(r);
                }
            }
            return resultsList;
        }

        public static bool UpdateItem(long id, string property, string value)
        {
            int r = 0;
            try
            {
                using (var context = new Context())
                {
                    decimal newValue = Convert.ToDecimal(value);
                    var result = context.Results.Include("Dealer").FirstOrDefault(x => x.Id == id);
                    if (result != null)
                    {
                        switch (property)
                        {
                            case CReport.OverallRetention:
                                var dealer = context.Dealers.Include("Reports").FirstOrDefault(x => x.DealerID == result.Dealer.DealerID);
                                var report = dealer.Reports.FirstOrDefault(x => x.Courtesy == CCourtesy.Dealer && result.MonthDateTime == x.MonthDateTime);
                                report.OverallRetention = newValue;
                                break;
                            case CResult.FirstYear:
                                result.FirstYr = newValue;
                                break;
                            case CResult.InWar:
                                result.InWar = newValue;
                                break;
                            case CResult.OutWar:
                                result.OutWar = newValue;
                                break;
                            case CResult.CustPayRoVsPriorYr:
                                result.CustPayRoVsPriorYr = newValue;
                                break;
                            case CResult.CustPayMoneyVsPriortYr:
                                result.CustPayMoneyVsPriortYr = newValue;
                                break;
                            case CResult.AbcNow:
                                result.AbcNow = decimal.ToInt32(newValue);
                                break;
                            case CResult.ZeroVisitNow:
                                result.ZeroVisitNow = decimal.ToInt32(newValue);
                                break;
                            case CResult.TwoVisitNow:
                                result.TwoVisitNow = decimal.ToInt32(newValue);
                                break;
                            case CResult.CrossNow:
                                result.CrossNow = decimal.ToInt32(newValue);
                                break;
                        }
                        r = context.SaveChanges();
                    }
                }
            }
            catch
            {
                r = 0;
            }
            return r > 0;
        }
    }
}
