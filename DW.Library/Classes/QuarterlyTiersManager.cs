﻿using System;
using System.Collections.Generic;
using System.Linq;
using DW.Library.DataModels;

namespace DW.Library.Classes
{
    public class QuarterlyTiersManager
    {
        public static QuarterlyTiersVM GetQuarterlyTiersForState(int month, int year, string stateAbbreviation)
        {
            QuarterlyTiersVM qt;
            var startDate = new DateTime(year, month, 2);
            using (var context = new Context())
            {
                // for this state get the rows that have startDate < curSelected month and year < endDate
                var latestQT = context.QuarterlyTierses
                    .Where(x => x.State == stateAbbreviation && x.StartDate < startDate && (x.EndDate > startDate || x.EndDate == null))
                    .OrderByDescending(x => x.StartDate).First();

                qt = new QuarterlyTiersVM();
                qt.QuarterlyTierID = latestQT.QuarterlyTierID;
                qt.State = latestQT.State;
                qt.StartDate = latestQT.StartDate;
                qt.EndDate = latestQT.EndDate;
                qt.INWT1 = latestQT.INWT1;
                qt.INWT2 = latestQT.INWT2;
                qt.OOWT1 = latestQT.OOWT1;
                qt.OOWT2 = latestQT.OOWT2;
                qt.CreateDate = latestQT.CreateDate;
                qt.ModifyDate = latestQT.ModifyDate;

            }
            return qt;
        }

        public static List<QuarterlyTiersVM> GetQuarterlyTiers(int month, int year)
        {
            List<QuarterlyTiersVM> list = new List<QuarterlyTiersVM>();
            var startDate = new DateTime(year, month, 1);
            using (var context = new Context())
            {
                // for every state get the rows that have startDate < curSelected month and year < endDate
                var statesGroups = context.QuarterlyTierses
                                    .Where(x => x.StartDate < startDate && (x.EndDate > startDate || x.EndDate == null))
                                    .GroupBy(x => x.State);
                foreach (var group in statesGroups)
                {
                    //if there are more than one row that is not passed endDate for the current selection (curSelected m and y < endDate)
                    //get the newest row
                    var latestQT = group.OrderByDescending(x => x.StartDate).First();
                    
                    var qt = new QuarterlyTiersVM();
                    qt.QuarterlyTierID = latestQT.QuarterlyTierID;
                    qt.State = latestQT.State;
                    qt.StartDate = latestQT.StartDate;
                    qt.EndDate = latestQT.EndDate;
                    qt.INWT1 = latestQT.INWT1;
                    qt.INWT2 = latestQT.INWT2;
                    qt.OOWT1 = latestQT.OOWT1;
                    qt.OOWT2 = latestQT.OOWT2;
                    qt.CreateDate = latestQT.CreateDate;
                    qt.ModifyDate = latestQT.ModifyDate;

                    list.Add(qt);
                }
            }
            return list;
        }
    }
}
