﻿using System;
using System.Collections.Generic;
using System.Linq;
using DW.Library.Constant;
using DW.Library.DataModels;

namespace DW.Library.Classes
{
    public class ReportManager
    {
        /// <summary>
        /// get all rows for this month (if a client doesn't have a row for this month one row will be created)
        /// </summary>
        public static List<ReportVM> GetReports(int month, int year)
        {
            List<ReportVM> reportsList = new List<ReportVM>();
            var selectedDate = new DateTime(year, month, 1);
            using (var context = new Context())
            {
                var dealers = context.Dealers.Include("Reports").ToList();
                foreach (var dealer in dealers)
                {
                    var reportD = dealer.Reports.FirstOrDefault(x => x.MonthDateTime == selectedDate && x.Courtesy == CCourtesy.Dealer);
                    if (reportD == null)
                    {
                        reportD = new Report();
                        reportD.MonthDateTime = selectedDate;
                        reportD.Dealer = dealer;
                        reportD.Courtesy = CCourtesy.Dealer;
                        context.Reports.Add(reportD);
                        context.SaveChanges();  // this is needed here for report id
                    }
                    AddReportRow(reportsList, reportD);

                    var reportR = dealer.Reports.FirstOrDefault(x => x.MonthDateTime == selectedDate && x.Courtesy == CCourtesy.Regional);
                    if (reportR == null)
                    {
                        reportR = new Report();
                        reportR.MonthDateTime = selectedDate;
                        reportR.Dealer = dealer;
                        reportR.Courtesy = CCourtesy.Regional;
                        context.Reports.Add(reportR);
                        context.SaveChanges();  // this is needed here for report id
                    }
                    AddReportRow(reportsList, reportR);
                }
            }
            return reportsList;
        }

        private static void AddReportRow(List<ReportVM> reportsList, Report report)
        {
            var row = new ReportVM();
            row.Id = report.Id;

            row.Dealer = new DealerVM();
            row.Dealer.Id = report.Dealer.DealerID;
            row.Dealer.Name = report.Dealer.DealerShipName;

            row.MonthDateTime = report.MonthDateTime;
            row.Courtesy = report.Courtesy;
            row.OverallRetention = report.OverallRetention;
            row.Visits4Plus = report.Visits4Plus;
            row.Visits3 = report.Visits3;
            row.Visits2 = report.Visits2;
            row.Visits1 = report.Visits1;
            row.Visits0 = report.Visits0;
            reportsList.Add(row);
        }

        public static bool UpdateItem(long id, string property, string value)
        {
            int r = 0;
            try
            {
                using (var context = new Context())
                {
                    decimal newValue = Convert.ToDecimal(value);
                    var result = context.Reports.FirstOrDefault(x => x.Id == id);
                    if (result != null)
                    {
                        switch (property)
                        {
                            case CReport.OverallRetention:
                                result.OverallRetention = newValue;
                                break;
                            case CReport.Visits4Plus:
                                result.Visits4Plus = newValue;
                                break;
                            case CReport.Visits3:
                                result.Visits3 = newValue;
                                break;
                            case CReport.Visits2:
                                result.Visits2 = newValue;
                                break;
                            case CReport.Visits1:
                                result.Visits1 = newValue;
                                break;
                            case CReport.Visits0:
                                result.Visits0 = newValue;
                                break;
                        }
                        r = context.SaveChanges();
                    }
                }
            }
            catch
            {
                r = 0;
            }
            return r > 0;
        }
    }
}
