﻿using System;
using System.Security.AccessControl;

namespace DW.Library.Classes
{
    public class DealerVM
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public Guid? StateID { get; set; }

        public int? OrigZeroVisitsCount { get; set; }
        public int? OrigTwoPlusCount { get; set; }

        public ResultVM Result { get; set; }

        public ResultVM InitialResult { get; set; }
        public ReportVM ReportD { get; set; }
        public ReportVM ReportR { get; set; }
        public ReportVM InitialReport { get; set; }
    }
}
