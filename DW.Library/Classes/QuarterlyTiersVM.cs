﻿using System;

namespace DW.Library.Classes
{
    public class QuarterlyTiersVM
    {
        public int QuarterlyTierID { get; set; }        
        public string State { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal INWT1 { get; set; }
        public decimal INWT2 { get; set; }
        public decimal OOWT1 { get; set; }
        public decimal OOWT2 { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
