﻿using System;
using System.Web.Script.Serialization;

namespace DW.Library.Classes
{
    public class ResultVM
    {
        public long Id { get; set; }
        [ScriptIgnore]
        public DealerVM Dealer { get; set; }
        public DateTime MonthDateTime { get; set; }

        //this prop is maped to Report.OverallRetention where Courtesy == CCourtesy.Dealer
        public decimal Overall { get; set; }
        public decimal FirstYear { get; set; }
        public decimal InWar { get; set; }
        public decimal OutWar { get; set; }
        public decimal CustPayRoVsPriorYr { get; set; }
        public decimal CustPayMoneyVsPriortYr { get; set; }

        public int AbcOrig { get; set; }
        public int AbcNow { get; set; }
        public int ZeroVisitOrig { get; set; }
        public int ZeroVisitNow { get; set; }
        public int TwoVisitNow { get; set; }
        public int TwoVisitOrig { get; set; }
        public int CrossOrig { get; set; }
        public int CrossNow { get; set; }

        public string ClientName => Dealer != null ? Dealer.Name : string.Empty;
        public long ClientId => Dealer != null ? Dealer.Id : -1;
    }
}
