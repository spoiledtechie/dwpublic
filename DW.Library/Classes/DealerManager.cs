﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DW.Library.Constant;
using DW.Library.DataModels;

namespace DW.Library.Classes
{
    public class DealerManager
    {
        public static DealerVM GetDealer(long dealerId, int month, int year)
        {
            DealerVM dealerVm = new DealerVM();
            var selectedDate = new DateTime(year, month, 1);
            using (var context = new Context())
            {
                var dealer = context.Dealers.Include("Reports").Include("Results").FirstOrDefault(x => x.DealerID == dealerId);
                dealerVm = MapDealer(dealer, selectedDate, context);
            }
            return dealerVm;
        }
        public static DealerVM GetDealer(long dealerId)
        {
            DealerVM dealerVm = new DealerVM();
            using (var context = new Context())
            {
                var dealer = context.Dealers.FirstOrDefault(x => x.DealerID == dealerId);
                //dealerVm = MapDealer(dealer, selectedDate, context);
            }
            return dealerVm;
        }

        public static DealerVM GetDealerInitial(long dealerId)
        {
            DealerVM dealerVm = new DealerVM();
            using (var context = new Context())
            {
                var dealer = context.Dealers.Include("Reports").Include("Results").FirstOrDefault(x => x.DealerID == dealerId);
                var minDate = dealer.Results.Min(x => x.MonthDateTime);
                dealerVm = MapDealer(dealer, minDate, context);
            }
            return dealerVm;
        }

        public static List<DealerVM> GetFullReport(int month, int year)
        {
            List<DealerVM> clientsList = new List<DealerVM>();
            var selectedDate = new DateTime(year, month, 1);
            using (var context = new Context())
            {
                var dealers = context.Dealers.Include("Reports").Include("Results").ToList();
                foreach (var dealer in dealers)
                {
                    var dealerVM = MapDealer(dealer, selectedDate, context);
                    clientsList.Add(dealerVM);
                }
            }
            return clientsList;
        }

        private static DealerVM MapDealer(Dealer dealer, DateTime selectedDate, Context context)
        {
            var dealerVM = new DealerVM();
            dealerVM.Id = dealer.DealerID;
            dealerVM.Name = dealer.DealerShipName;
            dealerVM.StateID = dealer.StateID;
            dealerVM.OrigZeroVisitsCount = dealer.OrigZeroVisitsCount;
            dealerVM.OrigTwoPlusCount = dealer.OrigTwoPlusCount;

            var reportD = dealer.Reports.FirstOrDefault(x => x.MonthDateTime == selectedDate && x.Courtesy == CCourtesy.Dealer);
            if (reportD == null)
            {
                reportD = new Report();
                reportD.MonthDateTime = selectedDate;
                reportD.Dealer = dealer;
                reportD.Courtesy = CCourtesy.Dealer;
                context.Reports.Add(reportD);
                context.SaveChanges();  // this is needed here for report id
            }
            dealerVM.ReportD = new ReportVM()
            {
                Id = reportD.Id,
                Dealer = dealerVM,
                MonthDateTime = reportD.MonthDateTime,
                Courtesy = reportD.Courtesy,
                OverallRetention = reportD.OverallRetention,
                Visits4Plus = reportD.Visits4Plus,
                Visits3 = reportD.Visits3,
                Visits2 = reportD.Visits2,
                Visits1 = reportD.Visits1,
                Visits0 = reportD.Visits0,
            };

            var reportR = dealer.Reports.FirstOrDefault(x => x.MonthDateTime == selectedDate && x.Courtesy == CCourtesy.Regional);
            if (reportR == null)
            {
                reportR = new Report();
                reportR.MonthDateTime = selectedDate;
                reportR.Dealer = dealer;
                reportR.Courtesy = CCourtesy.Regional;
                context.Reports.Add(reportR);
                context.SaveChanges();  // this is needed here for report id
            }
            dealerVM.ReportR = new ReportVM()
            {
                Id = reportR.Id,
                Dealer = dealerVM,
                MonthDateTime = reportR.MonthDateTime,
                Courtesy = reportR.Courtesy,
                OverallRetention = reportR.OverallRetention,
                Visits4Plus = reportR.Visits4Plus,
                Visits3 = reportR.Visits3,
                Visits2 = reportR.Visits2,
                Visits1 = reportR.Visits1,
                Visits0 = reportR.Visits0,

            };
            dealerVM.InitialReport = new ReportVM()
            {
                Dealer = dealerVM,
                MonthDateTime = dealer.CreatedDate,
                OverallRetention = dealer.OrigOverallRetention,
                Visits4Plus = dealer.FourPlusVisitsCount,
                Visits3 = dealer.ThreeVisitsCount,
                Visits2 = dealer.TwoVisitsCount,
                Visits1 = dealer.OneVisitsCount,
                Visits0 = dealer.ZeroVisitsCount,

            };

            var result = dealer.Results.FirstOrDefault(x => x.MonthDateTime == selectedDate);
            if (result == null)
            {
                result = new Result();
                result.MonthDateTime = selectedDate;
                result.Dealer = dealer;
                context.Results.Add(result);
                context.SaveChanges();  // this is needed here for result id
            }
            dealerVM.Result = new ResultVM
            {
                Id = result.Id,
                Dealer = dealerVM,
                MonthDateTime = result.MonthDateTime,
                CustPayRoVsPriorYr = result.CustPayRoVsPriorYr,
                CustPayMoneyVsPriortYr = result.CustPayMoneyVsPriortYr,
                AbcOrig = dealer.OrigACBRecordCount,
                CrossOrig = dealer.OriginalCSRecords,
                TwoVisitOrig = dealer.OrigTwoPlusCount ,
                ZeroVisitOrig = dealer.OrigZeroVisitsCount ,
                Overall = reportD.OverallRetention,
                FirstYear = result.FirstYr,
                InWar = result.InWar,
                OutWar = result.OutWar,

                AbcNow = result.AbcNow,
                ZeroVisitNow = result.ZeroVisitNow,
                TwoVisitNow = result.TwoVisitNow,
                CrossNow = result.CrossNow,
            };
            dealerVM.InitialResult = new ResultVM
            {
                Id = result.Id,
                Dealer = dealerVM,
                MonthDateTime = result.MonthDateTime,
                Overall = dealer.OrigOverallRetention,
                FirstYear = dealer.OrigFirstYearRetention,
                InWar = dealer.OrigInWRetention,
                OutWar = dealer.OrigOOWRetention,
                CustPayRoVsPriorYr = result.CustPayRoVsPriorYr,
                CustPayMoneyVsPriortYr = result.CustPayMoneyVsPriortYr,
                AbcOrig = dealer.OrigACBRecordCount,
                CrossOrig = dealer.OriginalCSRecords,
                TwoVisitOrig = dealer.OrigTwoPlusCount ,
                ZeroVisitOrig = dealer.OrigZeroVisitsCount ,

            };
            return dealerVM;
        }
    }
}
