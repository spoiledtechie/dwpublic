﻿namespace DW.Library.Classes
{
    public class ClientVM
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public ResultVM Result { get; set; }
        public ReportVM ReportD { get; set; }
        public ReportVM ReportR { get; set; }
    }
}
