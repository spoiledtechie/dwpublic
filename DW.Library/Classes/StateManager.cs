﻿using System;
using System.Linq;
using DW.Library.DataModels;

namespace DW.Library.Classes
{
    public class StateManager
    {
        public static StateVM GetState(Guid id)
        {
            StateVM stateVm = new StateVM();
            using (var context = new Context())
            {
                var state = context.States.FirstOrDefault(x => x.StateID == id);
                if (state != null)
                {
                    stateVm.StateID = state.StateID;
                    stateVm.StateName = state.StateName;
                    stateVm.Abbreviation = state.Abbreviation;
                }
            }
            return stateVm;
        }
    }
}
