﻿using System;

namespace DW.Library.Classes
{
    public class StateVM
    {
        public Guid StateID { get; set; }
        
        public string StateName { get; set; }
        public string Abbreviation { get; set; }
    }
}
