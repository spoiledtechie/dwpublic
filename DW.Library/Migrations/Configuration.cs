﻿using System;
using System.Data.Entity.Migrations;
using DW.Library.DataModels;

namespace DW.Library.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context context)
        {
            // var q = new QuarterlyTiers();
            // q.State = "FL";
            // q.StartDate = new DateTime(2016,9,1);
            // q.INWT1 = 62.36M;
            // q.INWT2 = 56.69M;
            // q.OOWT1 = 43.11M;
            // q.OOWT2 = 39.19M;
            // q.CreateDate = new DateTime(2016, 8, 1);
            // context.QuarterlyTierses.Add(q);

            // q = new QuarterlyTiers();
            // q.State = "FL";
            // q.StartDate = new DateTime(2016, 2, 1);
            // q.INWT1 = 6M;
            // q.INWT2 = 5M;
            // q.OOWT1 = 1M;
            // q.OOWT2 = 3M;
            // q.CreateDate = new DateTime(2016, 8, 1);
            // context.QuarterlyTierses.Add(q);

            // q = new QuarterlyTiers();
            // q.State = "CA";
            // q.StartDate = new DateTime(2016, 9, 1);
            // q.EndDate = new DateTime(2017,4,1);
            // q.INWT1 = 62.34M;
            // q.INWT2 = 56.67M;
            // q.OOWT1 = 41.88M;
            // q.OOWT2 = 38.07M;
            // q.CreateDate = new DateTime(2016, 7, 1);
            // context.QuarterlyTierses.Add(q);

            // q = new QuarterlyTiers();
            // q.State = "NY";
            // q.StartDate = new DateTime(2016, 9, 1);
            // q.INWT1 = 57.23M;
            // q.INWT2 = 52.03M;
            // q.OOWT1 = 41.21M;
            // q.OOWT2 = 37.46M;
            // q.CreateDate = new DateTime(2016, 8, 1);
            // context.QuarterlyTierses.Add(q);

            // context.SaveChanges();
        }
    }
}
