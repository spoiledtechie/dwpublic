namespace DW.Library.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updates : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.DW_Reports", "IX_MonthDateAndClient");
            AddColumn("dbo.DW_Reports", "Courtesy", c => c.String(nullable: false, maxLength: 1));
            CreateIndex("dbo.DW_Reports", new[] { "MonthDateTime", "ClientId", "Courtesy" }, unique: true, name: "IX_MonthDateAndClient");
        }
        
        public override void Down()
        {
            DropIndex("dbo.DW_Reports", "IX_MonthDateAndClient");
            DropColumn("dbo.DW_Reports", "Courtesy");
            CreateIndex("dbo.DW_Reports", new[] { "MonthDateTime", "ClientId" }, unique: true, name: "IX_MonthDateAndClient");
        }
    }
}
