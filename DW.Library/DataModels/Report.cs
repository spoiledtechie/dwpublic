﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DW.Library.DataModels
{
    [Table("DW_Reports")]
    public class Report
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        //[Index("IX_MonthDateAndClient", Order = 2, IsUnique = true)]
        public long DealerId { get; set; }
        public virtual Dealer Dealer { get; set; }
        //[Index("IX_MonthDateAndClient", Order = 1, IsUnique = true)]
        public DateTime MonthDateTime { get; set; }

        //[Required]
        //[StringLength(1, MinimumLength = 1)]
        //[Index("IX_MonthDateAndClient", Order = 3, IsUnique = true)]
        public string Courtesy { get; set; }
        public decimal OverallRetention { get; set; }
        public decimal Visits4Plus { get; set; }
        public decimal Visits3 { get; set; }
        public decimal Visits2 { get; set; }
        public decimal Visits1 { get; set; }
        public decimal Visits0 { get; set; }
    }
}
