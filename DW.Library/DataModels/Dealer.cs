﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DW.Library.DataModels
{
    [Table("Dealer")]
    public class Dealer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DealerID { get; set; }
        public string DealerShipName { get; set; }

        //prop mapped to DealerVM.InitialResult
        public int OrigACBRecordCount { get; set; }

        //prop mapped to DealerVM.InitialReport
        public int OrigOverallRetention { get; set; }
        //prop mapped to DealerVM.InitialResult
        public int OrigFirstYearRetention { get; set; }

        //props mapped to DealerVM.InitialResult
        public int OrigInWRetention { get; set; }
        public int OrigOOWRetention { get; set; }
        public int OriginalCSRecords { get; set; }
        //props mapped to DealerVM.InitialResult
        public int OrigZeroVisitsCount { get; set; }
        public int OrigTwoPlusCount { get; set; }

        public Guid? StateID { get; set; }

        //props mapped to DealerVM.InitialReport
        public decimal ZeroVisitsCount { get; set; }
        public decimal OneVisitsCount { get; set; }
        public decimal TwoVisitsCount { get; set; }
        public decimal ThreeVisitsCount { get; set; }
        public decimal FourPlusVisitsCount { get; set; }

        public int ZeroVisitsCustomerCount { get; set; }
        public int OneVisitsCustomerCount { get; set; }
        public int TwoVisitsCustomerCount { get; set; }
        public int ThreeVisistsCustomerCount { get; set; }
        public int FourPlusVisistsCustomerCount { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual ICollection<Result> Results { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
    }
}
