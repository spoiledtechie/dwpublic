﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DW.Library.DataModels
{
    [Table("DW_Results")]
    public class Result
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        //[Index("IX_MonthDateAndClient", Order = 2, IsUnique = true)]
        //public long DealerId { get; set; }
        public virtual Dealer Dealer { get; set; }
        //[Index("IX_MonthDateAndClient", Order = 1, IsUnique = true)]
        public DateTime MonthDateTime { get; set; }
        
        public decimal FirstYr { get; set; }
        public decimal InWar { get; set; }
        public decimal OutWar { get; set; }
        public decimal CustPayRoVsPriorYr { get; set; }
        public decimal CustPayMoneyVsPriortYr { get; set; }

        public int AbcNow { get; set; }
        public int ZeroVisitNow { get; set; }
        public int TwoVisitNow { get; set; }
        public int CrossNow { get; set; }
    }
}
