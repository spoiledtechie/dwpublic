﻿using System.Data.Entity;

namespace DW.Library.DataModels
{
    public class Context : DbContext
    {
        #region Public Members  
        public DbSet<State> States { get; set; }
        public DbSet<Dealer> Dealers { get; set; } 
        public DbSet<Report> Reports { get; set; }
        public DbSet<Result> Results { get; set; }
        public DbSet<QuarterlyTiers> QuarterlyTierses { get; set; }
        #endregion

        #region Public Methods
        public Context() : base("name=DWConnectionString")
        {
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Result>().Property(obj => obj.FirstYr).HasPrecision(5, 2);
            modelBuilder.Entity<Result>().Property(obj => obj.InWar).HasPrecision(5, 2);
            modelBuilder.Entity<Result>().Property(obj => obj.OutWar).HasPrecision(5, 2);
            modelBuilder.Entity<Result>().Property(obj => obj.CustPayRoVsPriorYr).HasPrecision(5, 2);
            modelBuilder.Entity<Result>().Property(obj => obj.CustPayMoneyVsPriortYr).HasPrecision(5, 2);

            modelBuilder.Entity<Report>().Property(obj => obj.OverallRetention).HasPrecision(5, 2);
            modelBuilder.Entity<Report>().Property(obj => obj.Visits4Plus).HasPrecision(5, 2);
            modelBuilder.Entity<Report>().Property(obj => obj.Visits3).HasPrecision(5, 2);
            modelBuilder.Entity<Report>().Property(obj => obj.Visits2).HasPrecision(5, 2);
            modelBuilder.Entity<Report>().Property(obj => obj.Visits1).HasPrecision(5, 2);
            modelBuilder.Entity<Report>().Property(obj => obj.Visits0).HasPrecision(5, 2);

            modelBuilder.Entity<QuarterlyTiers>().Property(obj => obj.INWT1).HasPrecision(4, 2);
            modelBuilder.Entity<QuarterlyTiers>().Property(obj => obj.INWT2).HasPrecision(4, 2);
            modelBuilder.Entity<QuarterlyTiers>().Property(obj => obj.OOWT1).HasPrecision(4, 2);
            modelBuilder.Entity<QuarterlyTiers>().Property(obj => obj.OOWT2).HasPrecision(4, 2);

            base.OnModelCreating(modelBuilder);
        }

        #endregion
    }
}
