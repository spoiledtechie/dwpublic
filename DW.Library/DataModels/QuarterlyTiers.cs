﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DW.Library.DataModels
{
    [Table("QuarterlyTiers")]
    public class QuarterlyTiers
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuarterlyTierID { get; set; }
        [Required]
        [MaxLength(50)]
        public string State { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [Required]
        public decimal INWT1 { get; set; }
        [Required]
        public decimal INWT2 { get; set; }
        [Required]
        public decimal OOWT1 { get; set; }
        [Required]
        public decimal OOWT2 { get; set; }
        [Required]
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
