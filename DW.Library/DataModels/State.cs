﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DW.Library.DataModels
{
    [Table("State")]
    public class State
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid StateID { get; set; }

        [StringLength(20)]
        public string StateName { get; set; }
        [StringLength(2)]
        public string Abbreviation { get; set; }
    }
}
