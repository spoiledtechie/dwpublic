﻿namespace DW.Library.Constant
{
    public struct CResult
    {
        public const string FirstYear = "firstYear";
        public const string InWar = "inWar";
        public const string OutWar = "outWar";
        public const string CustPayRoVsPriorYr = "custPayRoVsPriorYr";
        public const string CustPayMoneyVsPriortYr = "custPayMoneyVsPriortYr";

        public const string AbcNow = "AbcNow";
        public const string ZeroVisitNow = "ZeroVisitNow";
        public const string TwoVisitNow = "TwoVisitNow";
        public const string CrossNow = "CrossNow";
    }
}
