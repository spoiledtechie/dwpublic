﻿namespace DW.Library.Constant
{
    public struct CReport
    {
        public const string OverallRetention = "overallRetention";
        public const string Visits4Plus = "visits4Plus";
        public const string Visits3 = "visits3";
        public const string Visits2 = "visits2";
        public const string Visits1 = "visits1";
        public const string Visits0 = "visits0";
    }
}
