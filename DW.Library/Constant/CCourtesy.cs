﻿namespace DW.Library.Constant
{
    public struct CCourtesy
    {
        public const string Dealer = "D";
        public const string Regional = "R";
        
        public static string ToString(string courtesy)
        {
            string returnValue = null;
            if (courtesy.Equals(Dealer))
            {
                returnValue = "Dealer";
            }
            if (courtesy.Equals(Regional))
            {
                returnValue = "Regional";
            }
            return returnValue;
        }
    }
}
